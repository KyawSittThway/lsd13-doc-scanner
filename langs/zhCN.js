const Lang = {
    WS: {
        lowVer: '浏览器版本太低！请使用Chrome、Firefox、IE10+浏览器！',
        disConn: '未连接websocket服务器，请确保已运行服务端!',
    },
    MSG: {
        developing: '开发建设中...',
        mergePdfOk: '合并PDF成功.',
        ocrOk: 'OCR识别成功.',
        funcName: '方法名称为',
        usbKeyPress: 'USB按键按下,',
    },
    main: {
        upload: '上传返回值',
        base64: '拍照为Base64返回值',
        notOpen: '连接已断开或服务端未开启',
        disconnect: '断开连接'
    },
    cam: {
        bar: '条码识别中...',
        layoutRrecoing: '版面识别中...',
        doc: '高拍仪设备信息为空',
        stream: '视频流base64为空',
        res: '条码识别结果',
        content: '内容: ',
        type: '类型: ',
        noRrecognized: '没有识别到条码',
        errLayout: '版面识别错误'
    },
    autoCapture: {
        notStart: '自动连拍没有启动.',
        started: '自动连拍已启动, ',
        stopped: '自动连拍已停止, ',
        auto: '自动连拍.',
        timeLapse: '定时连拍.',
    },
    IDC: {
        name: '姓名: ',
        folk: '民族: ',
        gender: '性别: ',
        birthday: '出生日期: ',
        id: '身份证号: ',
        address: '地址: ',
        issue: '发证: ',
        fingerFeature: '指纹位置：',
        fingerFeature2: '指纹2位置：',
        fpPath: '指纹文件路径：',
        usefulLife: '有效期: ',
        startCompare: '开始比对',
        stopCompare: '停止比对',
        PPF: '请先准备好比对的头像文件。',
        PSF: '请先选择人像摄像头',
        FCP: '人脸比对结果:',
        Passed:'通过',
        unPassed:'不通过',
        livingPass: '活体检测通过, ',
        score: '分',
    },
    barcode: {
        promptInputPath: '请先输入要识别的【指定图片路径】',
        promptSelectFile: '请先选择要识别的图片文件',
    },
    scan: {
        base64Null: '扫描纸张base64为空',
        finish: '扫描已完成',
    }
}
