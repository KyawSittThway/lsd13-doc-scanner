const Lang = {
    WS: {
        lowVer: 'Browser version is too low! Please use chrome, Firefox, ie10 + browser!',
        disConn: 'Websocket server is not connected, please make sure the server is running!',
    },
    MSG: {
        developing: 'Under development and construction...',
        mergePdfOk: 'Merge PDF succeed.',
        ocrOk: 'OCR succeed.',
        funcName: 'The func name:',
        usbKeyPress: 'USB button Pressed,',
    },
    main: {
        upload: 'Upload return value',
        base64: 'Return value for Base64',
        notOpen: 'The connection is disconnected or the server is not opened',
        disconnect: 'Disconnect'
    },
    cam: {
        bar: 'Bar code recognition in progress...',
        layoutRrecoing: 'layout recognition in progress...',
        doc: 'Device information is empty',
        stream: 'Base64 video stream is empty',
        res: 'Results:',
        content: 'Content: ',
        type: 'Type: ',
        noRrecognized: 'No barcode recognized.',
        errLayout: 'layout recognition failed.'
    },
    autoCapture: {
        notStart: 'Auto capture is not start.',
        started: 'Auto capture is started.',
        stopped: 'Auto capture is stopped.',
        auto: 'Auto capture.',
        timeLapse: 'Time-lapse capture.',
    },
    IDC: {
        name: 'Name: ',
        folk: 'Folk: ',
        gender: 'Gender: ',
        birthday: 'Birthday: ',
        id: 'ID: ',
        address: 'Address: ',
        issue: 'Issue: ',
        usefulLife: 'Useful life: ',
        startCompare: 'Start compare',
        fingerFeature1: 'fingerFeature1: ',
        fingerFeature2: 'fingerFeature2: ',
        fpPath: 'fingerFeature path: ',
        stopCompare: 'Stop compare',
        PPF: 'Please prepare the right Face image file first.',
        PSF: 'Please select portrait camera first\'',
        FCP: 'Face comparison passed, similarity ',
        Passed: 'Pass',
        unPassed: 'not Pass',
        livingPass: 'Liveing check pass, ',
        score: 'score',
    },
    barcode: {
        promptInputPath: 'Please enter the [specified image path].',
        promptSelectFile: 'Please select the image file to recognize first.',
    },
    scan: {
        base64Null: 'Scan paper for Base64 is empty',
        finish: 'Scan finish',
    }
}
