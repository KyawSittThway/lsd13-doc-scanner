var _url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
var _connected = false;
var _devInfos = null;
var _devNum = -1;
var _supportOcr = 0;
var _cropBox = null;

/**
 * Initialize the webSocket connection
 */
function ConnectServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _connected = true;
        callback(value);
    }
    _ws.onclose = function (e) {
        _connected = false;
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            onTxtMessage(e.data);
        } else {
            onBinMessage(e);
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn)
    };
}

/**
 * Send information to the server
 */
function SendJson(json) {
    _connected ? _ws.send(JSON.stringify(json)) : ConnectServer(SendJson, json)
}

/**
 * 处理服务器二进制消息
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

/**
 * Process server binary messages
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the command is successfully executed. Other values are abnormal. 9 indicates that this exception is ignored when the camera is in use
        console.log(jsonObj.func, jsonObj.errorMsg);
        displayOutputInfo(3, jsonObj.func + "<br>" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                //Get OCR support information (language list)
                GetOcrInfo();
                GetVideoParam();
                break;
            case "GetOcrSupportInfo":
                DisplayOcrSupportInfo(jsonObj);
                break;
            case "CameraCapture":
                audioCapture.play();
                for (var i = 0; i < jsonObj.imagePath.length; i++) {
                    displayFile(jsonObj.imagePath[i]);
                }
                break;
            case "FileToBase64":
                DisplayFileView(jsonObj);
                break;
            case "MergeFile":
                DisplayMergeFile(jsonObj);
                break;
            case "FileOCR":
                DisplayConvertToFile(jsonObj);
                break;

            default:
                console.log(msg);
                break;
        }
    }
}

//The successful file recognition is displayed
function DisplayConvertToFile(jsonObj) {
    if (jsonObj.filePath) {
        displayOutputInfo(2, Lang.MSG.ocrOk + "<br>" + jsonObj.filePath);
        viewFile(jsonObj.filePath);
    }
}

//The merged PDF file is displayed
function DisplayMergeFile(jsonObj) {
    if (jsonObj.filePath) {
        displayOutputInfo(2, Lang.MSG.mergePdfOk + "<br>" + jsonObj.filePath);
        viewFile(jsonObj.filePath);
    }
}

function viewFile(filePath) {
    if (location.protocol.substr(0,4) == 'http') {
        var pos = filePath.lastIndexOf('\\');
        pos = filePath.lastIndexOf('\\', pos - 1);
        var url = location.origin + '/tmp/' + filePath.substr(pos + 1).replace('\\','/');
        window.open(url);
    }
}

var _path = null;
var _th = '<tr><th>File name</th><th>Zonal OCR</th><th>Operation</th></tr>';
var _tdOpration = '<td><a onclick="vf()">View</a> <a onclick="df()">Delete</a></td>';
function displayFile(file) {
    var pos = file.lastIndexOf('\\');
    if (_path == null) {
        _path = file.substring(0, pos);
    }
    var html = '<tr><td>' + file.substring(pos + 1) + '</td><td></td>' + _tdOpration + '</tr>';
    $("#tbFile").append(html);
}

// Clear the picture display list
function clearImageFile() {
    $("#image_data").empty();
    $("#tbFile").empty().append(_th);
}

function DisplayFileView(v) {
    var img = document.getElementById("imgFile");
    img.src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    // Show the cutting box
    _cropBox.enable();
}

function vf() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    $("#tbFile .active").removeClass("active");
    $(target).parent().parent().addClass("active");
    var file = _path + '\\' + $(target).parent().prev().prev().text();
    // Convert the file to Base64 for display viewing
    SendJson({func:'FileToBase64', reqId: new Date().getTime(), filePath:file});
}

// Deleting a photo file
function df() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    $(target).parent().parent().remove();
}

function displayOutputInfo(disp, s, add) {
    if (disp > 0 && disp < 4) {
        var ary = ["info", "success", "warning"];
        var h = "<b>" + s + "</b>";
        if (add) {
            var h0 = $('#outInfo').html();
            if (h0.length > 0) {
                h = h0 + "<br>" + h;
            }
        }
        $('#outInfo').css("display", "").attr("class", "col-md-12 alert alert-" + ary[disp - 1]).html(h);
    } else {
        $('#outInfo').css("display", "none").html("");
    }
}

var _w = 0, _h = 0;
function DisplayVideo(v) {
    if (v.imgBase64Str) {
        if (_w != v.width || _h != v.height) {
            //Set the width and height of the video display (400px)
            if (v.width > v.height) {
                $("#pic").css('width', _widthDivDev + 'px').css('height', Math.ceil(_widthDivDev * v.height / v.width) + 'px');
            } else {
                $("#pic").css('height', _widthDivDev + 'px').css('width', Math.ceil(_widthDivDev * v.width / v.height) + 'px');
            }
            _w = v.width, _h = v.height;
        }
        //Show video
        document.getElementById("pic").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    }
}

function DisplayDevInfo(devInfos) {
    _devInfos = devInfos;
    if (_devInfos == null || _devInfos.length == 0) {
        alert(Lang.cam.doc);
        return;
    }
    //Initialize device information
    displayCamera();
    displayMediaType();
    displayResolution();

    switchCameraInfo_Fun();
    setCameraImageInfo_Fun();
}

// Refresh the video format drop-down list
function displayMediaType() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var mediaTypes = _devInfos[index1].mediaTypes;
    var obj = document.getElementById("mediaType");
    obj.options.length = 0;
    if (_devInfos[index1].stillPin != undefined) {
        obj.options.add(new Option(_devInfos[index1].stillPin.mediaType, 0));
    } else {
        for (var i = 0; i < mediaTypes.length; ++i) {
            obj.options.add(new Option(mediaTypes[i].mediaType, i));
        }
    }
}

// Refresh the display resolution drop-down list
function displayResolution() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var index2 = document.getElementById("mediaType").options.selectedIndex;
    var obj = document.getElementById("resolution");
    // Clear resolution:SELECT data
    obj.options.length = 0;
    if (_devInfos[index1].stillPin != undefined) {
        _isStillPin = true;
        var SPresolutions = _devInfos[index1].stillPin.resolutions;
        for (var i = 0; i < SPresolutions.length; i++) {
            obj.options.add(new Option(SPresolutions[i], i));
        }
    } else {
        _isStillPin = false;
        var resolutions = _devInfos[index1].mediaTypes[index2].resolutions;
        for (var i = 0; i < resolutions.length; i++) {
            obj.options.add(new Option(resolutions[i], i));
        }
    }
}

//The onchange event for device information
function switchCameraInfo_Fun() {
    var reqId = new Date().getTime();
    if (_devNum >= 0) {
        //Turn off the previous camera
        SendJson({func:"CloseCamera", reqId: reqId, devNum: _devNum});
    }

    displayMediaType();
    displayResolution();

    // Turning on the Camera
    _devNum = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
    reqId++;
    SendJson({func:"OpenCamera", reqId: reqId, devNum: _devNum, mediaNum: 0, resolutionNum: 0, fps: 5});

    // Get a preview video
    reqId++;
    SendJson({func:"GetCameraVideoBuff", reqId: reqId, devNum: _devNum, enable: true});
}

function setCameraInfo_Fun() {
    var resolutionNum = $("#resolution").val() == null ? 0 : parseInt($("#resolution").val());
    var mediaNum = $("#mediaType").val() == null ? 0 : parseInt($("#mediaType").val());
    var reqId = new Date().getTime();
    if (_isStillPin) {
        SendJson({func: 'SetStillPinInfo', reqId: reqId, devNum: _devNum, resolutionNum: resolutionNum});
    } else {
        SendJson({func: 'SetCameraInfo', reqId: reqId, devNum: _devNum, mediaNum: mediaNum, resolutionNum: resolutionNum});
    }
}

function displayCamera() {
    var obj = document.getElementById("cameraInfo");
    var lastSelectedIndex = obj.options.selectedIndex;
    obj.options.length = 0;
    for (var i = 0; i < _devInfos.length; i++) {
        obj.options.add(new Option(_devInfos[i].devName, _devInfos[i].id));
    }
    if (lastSelectedIndex >= 0 && lastSelectedIndex < _devInfos.length) {
        obj.options.selectedIndex = lastSelectedIndex;
    }
}

function DisplayOcrSupportInfo(v) {
    if (v.languages) {
        // Support for ORC, allowing 'automatic text direction'
        _supportOcr = 1;
        document.getElementById('opOcrDirection').disabled = false;

        var obj = document.getElementById("language");
        obj.options.length = 0;
        for (var i = 0; i < v.languages.length; i++) {
            // 语言ID是从1开始
            obj.options.add(new Option(v.languages[i], i + 1));
        }
        // default select 'Simplified chinese+English'
        obj.selectedIndex = 98;

        if (v.fileTypes) {
            obj = document.getElementById("docType");
            obj.options.length = 0;
            for (var i = 0; i < v.fileTypes.length; i++) {
                obj.options.add(new Option(v.fileTypes[i], i));
            }
            obj.selectedIndex = 0;
        }
    } else {
        displayOutputInfo(3, "The system does not support the OCR function, and the OCR component may not be installed.");
    }
}

// Obtaining Device Information
function GetCameraInfo() {
    SendJson({func: "GetCameraInfo", reqId: new Date().getTime()});
}

// Get OCR support information
function GetOcrInfo() {
    SendJson({func: "GetOcrSupportInfo", reqId: new Date().getTime()});
}

function GetVideoParam() {
    SendJson({func: 'GetVideoParameter', reqId: new Date().getTime(), devNum: _devNum});
}

function cameraCapture_Fun() {
    setCameraImageInfo_Fun();
    SendJson({func: 'CameraCapture', reqId: new Date().getTime(), devNum: _devNum, mode: 'path'});
}

function setCameraImageInfo_Fun() {
    var cropType = parseInt($("#crop_type").val());
    var imageType = parseInt($("#image_type").val());
    var rotateAngle = parseInt($("#rotateType").val());
    var langId = parseInt($("#language").val());
    var reqId = new Date().getTime();
    var dpi = parseInt($("#dpi").val());
    var cmd = {func:'SetCameraImageInfo', reqId:reqId, devNum:_devNum, cropType:cropType, imageType:imageType,
        rotate:rotateAngle, textOrientationLangId:langId, dpi:dpi};
    SendJson(cmd);
}

function OnRotateChange() {
    setCameraImageInfo_Fun();
}

function mergePDF() {
    var reqId = new Date().getTime();
    var fileArray = new Array();
    var trs = $('#tbFile tr');
    for (i = 1; i < trs.length; i++) {
        fileArray.push(_path + '\\' + trs[i].children[0].innerText);
    }
    // fileType: 0 PDF; 1-TIFF(Temporary does not support)
    SendJson({func: 'MergeFile',reqId:reqId, fileType:0, imagePath:fileArray, mode:'path'});
}

function convertToFile() {
    var lang = $("#language option:selected").text();
    var docType = $("#docType option:selected").text();
    var reqId = new Date().getTime();
    var aryFile = new Array();
    var aryZones = new Array();
    var trs = $('#tbFile tr');
    for (i = 1; i < trs.length; i++) {
        aryFile.push(_path + '\\' + trs[i].children[0].innerText);

        var zones = new Array();
        var str = trs[i].children[1].innerText;
        var ary1 = str.split(";");
        for (var j = 0; j < ary1.length; j++) {
            var ary2 = ary1[j].split(",");
            if (ary2.length >= 6) {
                zones.push({left: ary2[0], top: ary2[1], right: ary2[2], bottom: ary2[3], absolute: ary2[4], type: ary2[5]});
            }
        }
        aryZones.push(zones);
    }
    SendJson({func: 'FileOCR', reqId: reqId, language: lang, extName: docType, DetectTextOrientation: false,
        imagePath: aryFile, zones: aryZones});
}

function cropBox_OnChange() {
    // Get the array of Cropboxes :[width of the video box, height of the video box, left of box 1, top of box 1, Right of box 1, Bottom of box 1, Left of box 2, top of box 2,....]
    var cropBoxArray = _cropBox.getCropBoxArray();
    var strVal = "";
    if (cropBoxArray.length >= 6) {
        var img = document.getElementById("imgFile");
        // 图片真实的尺寸
        var imgW = img.naturalWidth, imgH = img.naturalHeight;
        var ratio = imgW / cropBoxArray[0];
        var i = 2;
        while (i < cropBoxArray.length) {
            var left = Math.round(cropBoxArray[i++] * ratio);
            var top = Math.round(cropBoxArray[i++] * ratio);
            var right = Math.round(cropBoxArray[i++] * ratio);
            var bottom = Math.round(cropBoxArray[i++] * ratio);
            var rect = left + "," + top + "," + right + "," + bottom;
            if (strVal.length > 0) {
                strVal += ";";
            }
            var absolute = 1;
            var type = cropBoxArray[i++];
            strVal += (rect + "," + absolute + "," + type);
        }
    }
    $("#tbFile .active td:nth-child(2)").text(strVal);
    // console.log("cropBox_OnChange", cropBoxArray, img.width, img.height, strVal);
}

function cropBox_OnBuildInnerHtml(html) {
    var divHtml = '<div class="divZoneType"><label>Type:</label><select class="zoneType">' +
        '<option value="0">Text zones</option><option value="1">Graphical zones</option><option value="2">Table zones</option>' +
        '<option value="3">Barcode zones</option><option value="4">Handwritten zones</option></select></div>';
    return divHtml + html;
}

function cropBox_OnGetBoxVal(box, ary) {
    if (box) {
        var objSelects = box.getElementsByClassName("zoneType");
        if (objSelects.length > 0) {
            ary.push(parseInt(objSelects[0].value));
        }
    }
}

var _widthDivDev = 0, _widthMiddle = 0;
function onWinResize() {
    // Adjust page height
    var h = document.documentElement.clientHeight - document.getElementById("divTitle").offsetHeight;
    $("#divDev").css('height', h + 'px');
    $("#divTab").css('height', h + 'px');

    _widthDivDev = $("#divDev")[0].offsetWidth;
    _widthMiddle = document.documentElement.clientWidth - _widthDivDev - $("#divTab")[0].offsetWidth - 20;
    $("#pic").css('width', _widthDivDev + 'px').css("min-height", Math.ceil(_widthDivDev * 9/16) + 'px');
    $("#imgFile").css('width', _widthMiddle + 'px');

    if (_cropBox) {
        _cropBox.adjustPos();
    }
}

$(function () {
    GetCameraInfo();

    window.onresize = onWinResize;
    onWinResize();

    _cropBox = new CropBox("imgFile");
    _cropBox.onChange = cropBox_OnChange;
    _cropBox.onBuildInnerHtml = cropBox_OnBuildInnerHtml;
    _cropBox.onGetBoxVal = cropBox_OnGetBoxVal;
})