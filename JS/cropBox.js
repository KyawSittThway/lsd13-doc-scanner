var _instanceCB = null;
function CropBox(imgId){
    this.imgObj = document.getElementById(imgId);
    this.imgObj.draggable = false;
    // startX, startY Is the initial coordinate when the mouse clicks
    this.startX = -1, this.startY = -1;
    // diffX, diffY Is the initial mouse coordinates and cropBox The difference in coordinates of the upper left corner, used for dragging
    this.diffX = -1, this.diffY = -1;
    // Drag or not, initial is false
    this.dragging = false;
    // Video display area rect
    this.rect = {left:0, top:0, right:0, bottom:0};
    // Last location of the video display area
    this.lastPos = {x:0, y:0};
    this.overBox = null;
    this.onChange = null;
    this.onBuildInnerHtml = null;
    this.onGetBoxVal = null;
    this.enabled = false;
    _instanceCB = this;

    document.onselectstart = function() {
        return false;
    };

    this.enable = function(){
        // MouseDown
        document.onmousedown = function (e) {
            e = e || window.event;
            let t = e.target || e.srcElement;
            _instanceCB.getAreaRect();
            if (_instanceCB.inAreaRect(e)) {
                //console.log('imgObj.onmousedown inAreaRect', t.innerText);
                _instanceCB.startX = e.pageX;
                _instanceCB.startY = e.pageY;

                // If the mouse is pressed over cropBox
                if (e.target.className.match(/cropBox/)) {
                    // Allow Drag
                    _instanceCB.dragging = true;
                    //console.log('this.dragging = true');

                    // Set the current cropBox ID to moving_box
                    if (document.getElementById("moving_box") !== null) {
                        document.getElementById("moving_box").removeAttribute("id");
                    }
                    e.target.id = "moving_box";

                    // Compute the coordinate difference
                    _instanceCB.diffX = _instanceCB.startX - e.target.offsetLeft;
                    _instanceCB.diffY = _instanceCB.startY - e.target.offsetTop;
                } else if (_instanceCB.isOverBoxChild(t)) {
                    // Click on the cropBox child element
                } else {
                    // Create a cropBox on the page
                    let active_box = document.createElement("div");
                    active_box.id = "active_box";
                    active_box.className = "cropBox";
                    active_box.style.top = _instanceCB.startY + 'px';
                    active_box.style.left = _instanceCB.startX + 'px';
                    var html = '<a href="javascript:void(0)" onclick="_closeCropBox()" class="cropBox-close"></a>';
                    if (_instanceCB.onBuildInnerHtml) {
                        html = _instanceCB.onBuildInnerHtml(html);
                    }
                    active_box.innerHTML = html;
                    document.body.appendChild(active_box);
                    active_box = null;
                }
                e.stopPropagation();
            }
        };

        // mousemove
        document.onmousemove = function (e) {
            // console.log('onmousemove _instanceCB.dragging=', _instanceCB.dragging);
            if (_instanceCB.dragging) {
                // Move, update cropBox coordinates
                let mb = document.getElementById("moving_box");
                if (mb) {
                    //console.log('moving_box == _instanceCB.overBox', _instanceCB.overBox == mb);
                    //console.log(mb.style.cursor);
                    let l, t, w, h;
                    switch (mb.style.cursor) {
                        case 'n-resize':
                            t = e.pageY, h = mb.clientHeight + (mb.offsetTop - e.pageY);
                            if (h > 5 && t >= _instanceCB.rect.top && t < _instanceCB.rect.bottom - 5) {
                                mb.style.top = t + 'px';
                                mb.style.height = h + 'px';
                            }
                            break;
                        case 'ne-resize':
                            t = e.pageY, w = e.pageX - mb.offsetLeft, h = mb.clientHeight + (mb.offsetTop - e.pageY);
                            if (h > 5 && (mb.offsetTop + h) < _instanceCB.rect.bottom - 5 &&
                                t >= _instanceCB.rect.top && t < _instanceCB.rect.bottom - 5) {
                                mb.style.top = t + 'px';
                                mb.style.height = h + 'px';
                            }
                            if (w > 5 && (mb.offsetLeft + w) < _instanceCB.rect.right - 5) {
                                mb.style.width = w + 'px';
                            }
                            break;
                        case 'e-resize':
                            w = e.pageX - mb.offsetLeft;
                            if (w > 5 && (mb.offsetLeft + w) < _instanceCB.rect.right - 5) {
                                mb.style.width = w + 'px';
                            }
                            break;
                        case 'se-resize':
                            w = e.pageX - mb.offsetLeft, h = e.pageY - mb.offsetTop;
                            if (w > 5 && (mb.offsetLeft + w) < _instanceCB.rect.right - 5) {
                                mb.style.width = w + 'px';
                            }
                            if (h > 5 && (mb.offsetTop + h) < _instanceCB.rect.bottom - 5) {
                                mb.style.height = h + 'px';
                            }
                            break;
                        case 's-resize':
                            h = e.pageY - mb.offsetTop;
                            if (h > 5 && (mb.offsetTop + h) < _instanceCB.rect.bottom - 5) {
                                mb.style.height = h + 'px';
                            }
                            break;
                        case 'sw-resize':
                            w = mb.clientWidth + (mb.offsetLeft - e.pageX), l = e.pageX, h = e.pageY - mb.offsetTop;
                            if (l >= _instanceCB.rect.left && (l + w) < _instanceCB.rect.right - 5) {
                                mb.style.width = w + 'px';
                                mb.style.left = l + 'px';
                            }
                            if (h > 5 && (mb.offsetTop + h) < _instanceCB.rect.bottom - 5) {
                                mb.style.height = h + 'px';
                            }
                            break;
                        case 'w-resize':
                            w = mb.clientWidth + (mb.offsetLeft - e.pageX), l = e.pageX;
                            if (w > 5 && l >= _instanceCB.rect.left && (l + w) < _instanceCB.rect.right - 5) {
                                mb.style.width = w + 'px';
                                mb.style.left = l + 'px';
                            }
                            break;
                        case 'nw-resize':
                            if (e.pageY >= _instanceCB.rect.top && e.pageY < _instanceCB.rect.bottom - 5) {
                                mb.style.height = mb.clientHeight + (mb.offsetTop - e.pageY) + 'px';
                                mb.style.top = e.pageY + 'px';
                            }
                            if (e.pageX >= _instanceCB.rect.left && e.pageX < _instanceCB.rect.right - 5) {
                                mb.style.width = mb.clientWidth + (mb.offsetLeft - e.pageX) + 'px';
                                mb.style.left = e.pageX + 'px';
                            }
                            break;
                        case '':
                            l = e.pageX - _instanceCB.diffX, t = e.pageY - _instanceCB.diffY;
                            if (t >= _instanceCB.rect.top && (t + mb.offsetHeight) <= _instanceCB.rect.bottom) {
                                mb.style.top = t + 'px';
                            }
                            if (l >= _instanceCB.rect.left && (l + mb.offsetWidth) <= _instanceCB.rect.right) {
                                mb.style.left = l + 'px';
                            }
                            break;
                    }
                    e.stopPropagation();
                }
            } else {
                // Update cropBox size
                let ab = document.getElementById("active_box");
                if (ab) {
                    if (e.pageX >= _instanceCB.rect.left && e.pageX < _instanceCB.rect.right - 5) {
                        ab.style.width = e.pageX - _instanceCB.startX + 'px';
                    }
                    if (e.pageY >= _instanceCB.rect.top && e.pageY < _instanceCB.rect.bottom - 5) {
                        ab.style.height = e.pageY - _instanceCB.startY + 'px';
                    }
                } else {
                    if (_instanceCB.overBox && _instanceCB.overBox.style.cursor != '' && _instanceCB.inOverBox(e)) {
                        _instanceCB.overBox.style.cursor = '';
                    }
                }
            }
        };

        // MouseUP
        document.onmouseup = function (e) {
            //console.log('imgObj.onmouseup');
            // No Drag Move
            _instanceCB.dragging = false;
            if (document.getElementById("active_box") !== null) {
                let ab = document.getElementById("active_box");
                ab.removeAttribute("id");
                // If both sides are less than 5px, remove the cropBox
                if (ab.offsetWidth < 10 || ab.offsetHeight < 10) {
                    document.body.removeChild(ab);
                }
            }
            if (_instanceCB.onChange) {
                _instanceCB.onChange();
            }
        };

        document.onmouseover = function(e){
            e = e || window.event;
            let n = e.target || e.srcElement;
            if (window._lastNode != n && n.className == 'cropBox') {
                //console.log("onmouseover n=", n, "_instanceCB.overBox==null is ", _instanceCB.overBox==null);
                _instanceCB.overBox = n;
                let x = e.pageX - n.offsetLeft, y = e.pageY - n.offsetTop, dire = '';
                if (y >= 0 && y < 8) {
                    dire += 'n';
                } else if (y > n.offsetHeight - 8 && y < n.offsetHeight + 8) {
                    dire += 's';
                }
                if (x >= 0 && x < 8) {
                    dire += 'w';
                } else if (x > n.offsetWidth - 8 && x < n.offsetWidth + 8) {
                    dire += 'e';
                }
                //console.log(dire + '-resize');
                n.style.cursor = dire + '-resize';
                n.className += ' hover';
                //console.log('add hover');
            } else {
                if (_instanceCB.overBox) {
                    if (n.parentNode != _instanceCB.overBox && n != _instanceCB.overBox && !_instanceCB.isOverBoxChild(n)) {
                        _instanceCB.overBox.className = 'cropBox';
                        _instanceCB.overBox.cursor = '';
                        //console.log('onmouseover remove hover  _instanceCB.overBox = null');
                        _instanceCB.overBox = null;
                    }
                }
            }
        };

        this.enabled = true;
        this.displayCropBox('');
    };

    this.disable = function(){
        document.onmousedown = null;
        document.onmousemove = null;
        document.onmouseup = null;
        this.enabled = false;
        this.displayCropBox('none');
    };

    this.displayCropBox = function(val) {
        let ary = document.getElementsByClassName('cropBox');
        for (let i = 0; i < ary.length; i++) {
            ary[i].style.display = val;
        }
    };

    this.getAreaRect = function() {
        let e = this.imgObj, left = e.offsetLeft, top = e.offsetTop;
        while ((e = e.offsetParent) != null) {
            top += e.offsetTop;
            left += e.offsetLeft;
        }
        this.rect.left = left;
        this.rect.top = top;
        this.rect.right = left + this.imgObj.offsetWidth;
        this.rect.bottom = top + this.imgObj.offsetHeight;
    };

    // Used to adjust the position of the cutting box after the position of the video box changes
    this.adjustPos = function() {
        if (this.enabled) {
            let x = this.rect.left, y = this.rect.top;
            this.getAreaRect();
            x = this.rect.left - x, y = this.rect.top - y;
            let boxes = document.getElementsByClassName('cropBox'), box = null, left, top;
            for (let j = 0; j < boxes.length; j++) {
                box = boxes[j];
                box.style.left = (parseInt(box.style.left) + x) + 'px';
                box.style.top = (parseInt(box.style.top) + y) + 'px';
            }
        }
    }

    this.inAreaRect = function(e) {
        return e.pageX >= this.rect.left && e.pageX <= this.rect.right && e.pageY >= this.rect.top && e.pageY <= this.rect.bottom;
    };

    this.inOverBox = function(e) {
        if (this.overBox) {
            let x = e.pageX - this.overBox.offsetLeft, y = e.pageY - this.overBox.offsetTop;
            return x > 8 && y > 8 && x < (this.overBox.offsetWidth - 8) && y < (this.overBox.offsetHeight - 8);
        }
        return false;
    };

    this.isOverBoxChild = function(obj) {
        if (this.overBox) {
            while (obj != undefined && obj != null && obj.tagName.toUpperCase() != 'BODY'){
                if (obj == this.overBox) {
                    return true;
                }
                obj = obj.parentNode;
            }
        }
        return false;
    };

    // Get array of CropBoxes :[width of video box, height of video box, left of box 1, top of box 1, right of box 1, bottom of box 1, left of box 2, top of box 2,....]
    // The position of each box, starting from the top left corner of the video box (0,0)
    this.getCropBoxArray = function() {
        this.getAreaRect();
        let ary = new Array(), i = 0;
        ary[i++] = this.imgObj.offsetWidth;
        ary[i++] = this.imgObj.offsetHeight;

        let boxes = document.getElementsByClassName('cropBox'), box = null, left, top;
        for (let j = 0; j < boxes.length; j++) {
            box = boxes[j];
            left = box.offsetLeft - this.rect.left;
            top = box.offsetTop - this.rect.top;
            ary[i++] = left;
            ary[i++] = top;
            ary[i++] = left + box.clientWidth;
            ary[i++] = top + box.clientHeight;
            if (this.onGetBoxVal) {
                this.onGetBoxVal(box, ary);
                i = ary.length;
            }
        }
        return ary;
    };
}

function _closeCropBox(e) {
    e = e || window.event;
    let a = e.target || e.srcElement;
    document.body.removeChild(a.parentNode);
    if (_instanceCB.onChange) {
        _instanceCB.onChange();
    }
}