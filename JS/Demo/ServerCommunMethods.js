
/**
 * Camera control messages
 */
function GetCameraInfo() {
    sendTxt(`{"func":"GetCameraInfo","reqId":"${new Date().getTime()}"}`);
}

function OpenCamera() {
    sendTxt(`{"func":"OpenCamera", "reqId" : "${new Date().getTime()}" }`)
}

function CloseCamera() {
    sendTxt(`{"func":"CloseCamera", "reqId" : "${new Date().getTime()}"}`);
}


