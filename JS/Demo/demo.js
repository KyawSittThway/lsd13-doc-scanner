const _url = ((location.protocol == "https:") ? "wss://" : "ws://") + ((location.hostname == "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "" ? '9000' : location.port));
var _ws;
var _wsConnected = false;
var _deviceInfos = null;
var _deviceNumber = -1;
var _supportOcr = 0;
var _cropBox = null;
var _autoCaptureTimer = 0;
var _detectionType = -1; //auto capture type.
var _mediaType = "MJPG";
var _rotateAngle = 0;
var _currentDeviceInfoId = 0;
var _zoom = 0;

var outputFolder = "C:\\Users\\kyawsittthway\\Documents\\Doc Scanner Images\\"

$(() => {
    GetCameraInfo();

    window.onresize = onWindowResize;
    // onWindowResize();

    _cropBox = new CropBox("img-preview");
})

/* 
 * Web socket connection initilization 
 */
function ConnectWsServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (windows.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }

    _ws.onopen = () => {
        _wsConnected = true;
        callback(value);
    }

    _ws.onclose = () => {
        _wsConnected = false;
    }

    _ws.onmessage = (e) => {
        typeof e.data === "string" ? onTxtMessage(e.data) : onBinMessage(e.data);
    }
}

function onWindowResize() {
    // Initialize the height of the img-previewture list
    // $("#right_div").css('height', (document.body.clientHeight - 30) + 'px');
    // Initialize 'result display' height
    // var p = $("#resultArea").offset();
    // $("#resultArea").css('height', (document.body.clientHeight - p.top - 16) + 'px');

    var cropType = parseInt($("#crop_type").val());

    // Flatten surface requires a middleline
    if (cropType == 3) {
        showMiddleLine_Fun(cropType);
    }

    if (_cropBox) {
        _cropBox.adjustPos();
    }
}

/**
 * Sends information to the server
 */
function sendText(jsonStr) {
    _wsConnected ? _ws.send(jsonStr) : ConnectWsServer(sendText, jsonStr);
}

function sendJson(json) {
    _wsConnected ? _ws.send(JSON.stringify(json)) : ConnectWsServer(sendJson, json);
}

/**
 * Process received server text messages
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the execution is successful. If other values are returned, the execution is abnormal. 
        //  9: The device is in use. Ignore this exception when you start the camera
        console.error(jsonObj.func, jsonObj.errorMsg);
        alert(jsonObj.func + "\r\n" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayPreviewVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                break;
            case "CameraCapture":
                // for (var i = 0; i < jsonObj.imgBase64.length; i++) {
                //     DisplayCapturedImage(jsonObj.mime, jsonObj.imgBase64[i]);
                // }
                console.log("Output Folder : " + CONFIGURATION.outputFolder);
                console.log("Capture Image Json : ");
                console.log(jsonObj);
                break;
            case "Notify":
                // receipt notify
                if (jsonObj.event == "OnUsbKeyPress") {
                    // Received notification of USB button press
                    // {"func":"Notify","event":"OnUsbKeyPress","reqId":1636099207839.0,"result":0,"time":"1636099214"}
                    DisplayUsbKeyPress(jsonObj);
                } else if (jsonObj.event == "OnDeviceChanged") {
                    // The format of the device change is as follows:
                    // {"func":"Notify","event":"OnDeviceChanged","devName":"\\\\?\\USB#VID_0C45&PID_6366...","func":"OnDeviceChanged","result":0,"type":1}
                    // type: 1-Device insertion; 2-Device removed
                    GetCameraInfo();
                }
                break;
            case "CameraVideoParam":
                console.log("CameraVideoParam Response");
                console.log(jsonObj);
            default:
                console.log(msg);
                break;
        }
    }
}

/**
 * Process received server binary messages
 */
function onBinMessage(msg) {
    console.log("Server Binary message : " + msg);
}


/**
 * Display camera live feed
 */
var _w = 0, _h = 0;
function DisplayPreviewVideo(v) {
    if (v.imgBase64Str) {
        if (_w != v.width || _h != v.height) {
            // Sets the width and height of the video control(Width control is 640px)
            if (v.width > v.height) {
                $("#img-preview").css('width', '640px').css('height', 640 * v.height / v.width + 'px');
            } else {
                $("#img-preview").css('height', '640px').css('width', 640 * v.width / v.height + 'px');
            }
            _w = v.width, _h = v.height;
        }
        // show video
        document.getElementById("img-preview").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    }
}


/**
 * Camera control messages
 */
function GetCameraInfo() {
    sendText(`{"func":"GetCameraInfo","reqId":"${new Date().getTime()}"}`);
}

function OpenCamera() {
    sendText(`{"func":"OpenCamera", "reqId" : "${new Date().getTime()}", "devNum" :"${_deviceNumber}" , "mediaNum" :0, "resolutionNum":0, "fps":60}`)
}

function CloseCamera() {
    sendText(`{"func":"CloseCamera", "reqId" : "${new Date().getTime()}"}`);
}

function GetCameraVideoBuff() {
    sendText(`{"func" : "GetCameraVideoBuff", "reqId" : "${new Date().getTime()}", "devNum" : "${_deviceNumber}", "enable" :"true"}`)
}

function ZoomIn_Func() {
    let reqId = new Date().getTime();
    _zoom += 1;
    console.log(`Zoom level : ${_zoom}`);
    let cmd = `{ "func": "CameraVideoParam", "reqId": ${reqId}, "devNum": ${_deviceNumber}, "items": [{ "id": 103, "val": ${_zoom}, "flag": 2}]}`
    sendText(cmd);
}

function ZoomOut_Func() {
    let reqId = new Date().getTime();
    if (_zoom <= 0) {
        alert("Cannot not zoom out any more!");
        return;
    }
    _zoom -= 10;
    sendText(JSON.stringify({
        func: "CameraVideoParam", reqId: reqId, devNum: _deviceNumber, items: [
            {
                id: 103,
                flag: 2,
                val: _zoom
            }
        ]
    }))
}

function DisplayDevInfo(devInfos) {
    _deviceInfos = devInfos;
    if (_deviceInfos.length == 0) {
        alert(Lang.cam.doc);
        return;
    }

    // Filter the Integrate camera in Laptop if it exists
    _deviceInfos = _deviceInfos.filter(device => {
        if (device.devName.toLowerCase() !== "integrated camera") {
            return device;
        }
    })
    _deviceNumber = _deviceInfos[_currentDeviceInfoId].id;

    // setMediaType();
    setDeviceResolution();
    switchCamera();
}

// Refresh the display video format drop-down list
// function setMediaType() {
//     // let mediaTypes = _deviceInfos[_currentDeviceInfoId].mediaTypes;
//     // let obj = document.getElementById("mediaType");
//     // obj.options.length = 0;
//     // for (let i = 0; i < mediaTypes.length; ++i) {
//     //     obj.options.add(new Option(mediaTypes[i].mediaType, i));
//     // }
//     let index = _deviceInfos[_currentDeviceInfoId].mediaTypes.indexOf("MJPG");
//     _mediaType = _deviceInfos[_currentDeviceInfoId].mediaTypes[index];
// }

// Refresh the display resolution drop-down list
function setDeviceResolution() {
    let mediaTypeIndex = 0;
    _deviceInfos[_currentDeviceInfoId].mediaTypes.forEach((media, index) => {
        if (media.mediaType == "MJPG")
            mediaTypeIndex = index;
    });

    let resolutions = _deviceInfos[_currentDeviceInfoId].mediaTypes[mediaTypeIndex].resolutions;

    let obj = document.getElementById("resolution");
    obj.options.length = 0;
    for (var i = 0; i < resolutions.length; i++) {
        obj.options.add(new Option(resolutions[i], i));
    }
}

// Onchange event of device information 
function switchCamera() {
    var reqId = new Date().getTime();
    if (_deviceNumber >= 0) {
        // close the old cameras
        sendText('{"func":"CloseCamera","reqId":"' + reqId + '","devNum":' + _deviceNumber + '}');
    }

    // Refresh the display video format drop-down list
    // setMediaType();
    // Refresh the display resolution drop-down list
    setDeviceResolution();

    // Open camera
    reqId++;
    var cmd = '{"func":"OpenCamera","reqId":"' + reqId + '","devNum":' + _deviceNumber + ',"mediaNum":0,"resolutionNum":0,"fps":5}';
    sendText(cmd);

    // Get preview video
    reqId++;
    cmd = '{"func":"GetCameraVideoBuff","reqId":"' + reqId + '","devNum":' + _deviceNumber + ',"enable":"true"}';
    sendText(cmd);
}

// Controls the display of custom cutting frames
function allowCropBox(cropType) {
    if (cropType == 9) {
        _cropBox.enable();
    } else {
        _cropBox.disable();
    }
}




function rotateImg() {
    // if (_supportOcr) {
    //     $("#language").parent().css('display', document.getElementById("rotateType").selectedIndex == 4 ? '' : 'none');
    //     var p = $("#resultArea").offset();
    //     $("#resultArea").css('height', (document.body.clientHeight - p.top - 10) + 'px');
    // }
    console.log("Current Angle " + _rotateAngle);
    if (_rotateAngle == 270) {
        _rotateAngle = 0;
    } else {
        _rotateAngle += 90;
    }

    setCameraImageInfo_Fun();
}

/**
 * Image capture functions
 */
// // Add an image item
// function DisplayCapturedImage(mime, imgBase64Str) {
//     var h = '<div>';
//     h += '<a href="javascript:void(0)" onclick="delP()" class="img-del"></a>';
//     h += '<img width="330" src="' + 'data:' + mime + ';base64,' + imgBase64Str + '">';
//     h += '</div>';
//     $("#image_data").prepend(h);
// }

// // delete image file
// function delP() {
//     var e = e || window.event;
//     var target = e.target || e.srcElement;
//     $(target).parent().remove();
// }

// // Clear the image display list
// function clearImageFile() {
//     $("#image_data").empty();
// }

/** 
 * Event functions 
 * **/
function setCameraImageInfo_Fun() {
    var reqId = new Date().getTime();

    var cropType = parseInt($("#crop_type").val());
    // Controls the display of custom cutting frames
    allowCropBox(cropType);
    var imageType = parseInt($("#image_type").val());
    var fillBorderType = parseInt($("#fill_border").val());
    // var removalForeign = parseInt($("#foreign_remove").val());
    var rotateAngle = _rotateAngle;

    var cropBoxArray = null;
    if (cropType == 9) {
        cropBoxArray = _cropBox.getCropBoxArray();
        if (cropBoxArray.length < 6) {
            // If custom cutting is set, but no cutting frame is drawn, press "No cutting"
            cropType = 0;
            cropBoxArray = null;
        }
    }

    var cmd = {
        func: 'SetCameraImageInfo',
        reqId: reqId,
        devNum: _deviceNumber,
        cropType: cropType,
        imageType: imageType,
        fillBorderType: fillBorderType,
        // removalForeign: removalForeign,
        rotate: rotateAngle
    };
    if (cropBoxArray) {
        cmd.posArray = cropBoxArray;
    }

    sendText(JSON.stringify(cmd));
}

function captureImage_Fun() {
    // Set image algorithm
    setCameraImageInfo_Fun();
    // Camera capture
    var reqId = new Date().getTime();
    var fileExtension = $("#FileType").find(":selected").text();
    console.log("File Extension : " + fileExtension);
    var imageLocation = outputFolder + reqId + "." + fileExtension;
    var cmd = JSON.stringify({ func: 'CameraCapture', reqId: reqId, devNum: _deviceNumber, mode: 'base64;path', imagePath: imageLocation });
    sendText(cmd);
}

function SwitchCamera_Fun() {
    let reqId = new Date().getTime();
    // Close current camera
    if (_deviceNumber >= 0) {
        sendText(JSON.stringify({ func: "CloseCamera", reqId: reqId, devNum: _deviceNumber }));
    }

    // setMediaType();
    setDeviceResolution();

    // Switch cameras
    reqId++;
    console.log(_deviceNumber);
    if (_currentDeviceInfoId == 0) {
        let deviceNumber = _deviceInfos[_currentDeviceInfoId].id;
        sendText(JSON.stringify({ func: "OpenCamera", reqId: reqId, mediaNum: 0, resolutionNum: 0, fps: 60, devNum: deviceNumber }));
        _deviceNumber = deviceNumber;
        _currentDeviceInfoId = 1;
    } else {
        let deviceNumber = _deviceInfos[_currentDeviceInfoId].id;
        sendText(JSON.stringify({ func: "OpenCamera", reqId: reqId, mediaNum: 0, resolutionNum: 0, fps: 60, devNum: deviceNumber }));
        _deviceNumber = deviceNumber;
        _currentDeviceInfoId = 0;
    }

    // Preview video of the switched camera
    reqId++;
    console.log("Current Dev Info Id" + _currentDeviceInfoId);
    sendText(JSON.stringify({ func: "GetCameraVideoBuff", reqId: reqId, devNum: _deviceNumber, enable: true }))

    console.log("Switched camera");
}

