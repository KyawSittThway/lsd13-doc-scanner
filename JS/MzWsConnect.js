// Encapsulates the webSocket connection
function MzWsConnect(port, onTxtMsg, onError, beforeSend) {
    this.port = port;
    this.ws = null;
    this.connectState = 0; // 0 is not connected. 1 Connecting; 2 the connected
    this.onTxtMsg = onTxtMsg;
    this.onError = onError;
    this.beforeSend = beforeSend;
    this.waitSendMsgs = [];

    this.Open = function () {
        if (this.connectState == 0) {
            this.connectState = 1;
            var that = this;
            let url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
            if ('WebSocket' in window) {
                this.ws = new WebSocket(url);
            } else if (window.WebSocket) {
                this.ws = new WebSocket(url);
            } else if ('MozWebSocket' in window) {
                this.ws = new MozWebSocket(url);
            } else {
                alert(Lang.WS.lowVer);
            }
            this.ws.onopen = function (e) {
                that.connectState = 2;
                console.log(e.target.url + ' connect succeed', e);
                if (that.waitSendMsgs.length > 0) {
                    for (var i = 0; i < that.waitSendMsgs.length; i++) {
                        that.SendJson(that.waitSendMsgs[i]);
                    }
                    that.waitSendMsgs = [];
                }
            }
            this.ws.onclose = function (e) {
                that.connectState = 0;
            }
            this.ws.onmessage = function (e) {
                if (typeof e.data === "string") {
                    that.onTxtMsg(e.data, that.port);
                } else {
                    console.log('onBinMessage', e);
                }
            }
            this.ws.onerror = function (e) {
                console.log('onerror', e);
                if (typeof (that.onError) === "function") {
                    that.onError(e, Lang.WS.disConn);
                }
            };
        }
    };

    this.Close = function () {
        if (this.connectState > 0) {
            this.ws.close();
        }
    };

    this.SendJson = function (jsonMsg) {
        if (this.connectState == 2) {
            if (this.beforeSend) {
                this.beforeSend();
            }
            this.ws.send(JSON.stringify(jsonMsg));
        } else {
            this.waitSendMsgs.push(jsonMsg);
            if (this.connectState == 0) {
                this.Open();
            }
        }
    };
};