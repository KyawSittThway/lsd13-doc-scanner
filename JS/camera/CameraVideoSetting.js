var _url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
var _connected = false;
var _devInfos = null;
var _supportOcr = 0;
var _devNum = -1;
var _items = null;

/**
 * init webSocket connect
 */
function ConnectServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _connected = true;
        callback(value);
    }
    _ws.onclose = function (e) {
        _connected = false;
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            onTxtMessage(e.data);
        } else {
            onBinMessage(e);
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn);
    };
}

/**
 * Sends information to the server
 */
function SendTxt(jsonStr) {
    _connected ? _ws.send(jsonStr) : ConnectServer(SendTxt, jsonStr)
}
function SendJson(json) {
    _connected ? _ws.send(JSON.stringify(json)) : ConnectServer(SendJson, json)
}
/**
 * Process server binary messages
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

/**
 * Process received server text messages
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the execution is successful. If other values are returned, the execution is abnormal.
        // 9: The device is in use. Ignore this exception when you start the camera
        console.log(jsonObj.func, jsonObj.errorMsg);
        alert(jsonObj.func + "\r\n" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                // Get OCR support information (list of languages)
                GetOcrInfo();
                break;
            case "CameraVideoParam":
                DisplayCameraVideoParam(jsonObj);
                break;
            case "CameraCapture":
                DisplayCaptureImg(jsonObj);
                break;
            case "GetOcrSupportInfo":
                DisplayOcrSupportInfo(jsonObj);
                break;
            case "FileToBase64":
                DisplayFileView(jsonObj);
                break;
            case "MergeFile":
                DisplayMergeFile(jsonObj);
                break;
            case "Notify":
                // receipt of notification
                if (jsonObj.event == "OnUsbKeyPress") {
                    // Received notification of USB button press
                    // {"func":"Notify","event":"OnUsbKeyPress","reqId":1636099207839.0,"result":0,"time":"1636099214"}
                    // DisplayUsbKeyPress(jsonObj);
                } else if (jsonObj.event == "OnDeviceChanged") {
                    // The format of the device is as follows:
                    // {"func":"Notify","event":"OnDeviceChanged","devName":"\\\\?\\USB#VID_0C45&PID_6366...","func":"OnDeviceChanged","result":0,"type":1}
                    // type: 1 Device insertion; 2 The device is removed
                    GetCameraInfo();
                }
                break;
            case "FileOCR":
                DisplayConvertToFile(jsonObj);
                break;
            default:
                console.log(msg);
                break;
        }
    }
}

// Get device info
function GetCameraInfo() {
    SendJson({func: "GetCameraInfo", reqId: new Date().getTime()});
}

// Get OCR support information
function GetOcrInfo() {
    SendJson({func: "GetOcrSupportInfo", reqId: new Date().getTime()});
}

// Displays the merged PDF file
function DisplayMergeFile(jsonObj) {
    if (jsonObj.filePath) {
        alert(Lang.MSG.mergePdfOk + "\r\n" + jsonObj.filePath);
        if (location.protocol.substr(0,4) == 'http') {
            var pos = jsonObj.filePath.lastIndexOf('\\');
            pos = jsonObj.filePath.lastIndexOf('\\', pos - 1);
            var url = location.origin + '/tmp/' + jsonObj.filePath.substr(pos + 1).replace('\\','/');
            window.open(url);
        }
    }
}

// The file that was successfully identified is displayed
function DisplayConvertToFile(jsonObj) {
    if (jsonObj.filePath) {
        alert(Lang.MSG.mergePdfOk + "\r\n" + jsonObj.filePath);
        var pos = jsonObj.filePath.lastIndexOf('\\');
        pos = jsonObj.filePath.lastIndexOf('\\', pos - 1);
        var url = location.origin + '/tmp/' + jsonObj.filePath.substr(pos + 1).replace('\\','/');
        window.open(url);
    }
}

var _w = 0, _h = 0, _wp = 0, _hp = 0;
function DisplayVideo(v) {
    if (v.imgBase64Str) {
        if (_w != v.width || _h != v.height) {
            // Set the width and height of the video display (height control: 484px)
            _hp = 484, _wp = _hp * v.width / v.height, _w = v.width, _h = v.height;
            $("#pic").css('width', _wp + 'px').css('height', _hp + 'px');
        }

        //显示视频
        document.getElementById("pic").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    }
}

function DisplayDevInfo(devInfos) {
    _devInfos = devInfos;
    if (_devInfos.length == 0) {
        alert(Lang.cam.doc);
        return;
    }
    // Initialize device information
    displayCamera();
    switchCameraInfo_Fun();
}

function DisplayOcrSupportInfo(v) {
    if (v.languages) {
        _supportOcr = 1;

        var obj = document.getElementById("languages");
        obj.options.length = 0;
        for (var i = 0; i < v.languages.length; i++) {
            obj.options.add(new Option(v.languages[i], i));
        }
        // Default 'Simplified chinese+English'
        obj.selectedIndex = 98;

        obj = document.getElementById("extNames");
        obj.options.length = 0;
        for (var i = 0; i < v.fileTypes.length; i++) {
            obj.options.add(new Option(v.fileTypes[i], i));
        }
    }
}

function buildTr(p) {
    // capsFlag: 0 is not supported. 1 is automatic, 2 is manual, and 3 is configurable
    var support = p.capsFlag > 0;
    // flag: 1:Auto，2:Manual
    var html = '<tr pid="' + p.id + '">';
    html += '<td>' + p.name + '</td>';
    html += '<td><input type="range" class="valAdj" min="' + p.min + '" max="' + p.max + '" value="' + p.val + '" step="' + p.step + '"';
    if (!support || p.flag == 1) {
        html += ' disabled';
    }
    html += '></td>';
    html += '<td><input type="number" class="valDisp" disabled';
    if (support) {
        html += ' value="' + p.val + '"';
    }
    html += '></td>';
    html += '<td><input type="checkbox" class="autoChk"';
    if (support && p.flag == 1) {
        html += ' checked';
    }
    if (!support || p.capsFlag != 3) {
        html += ' disabled';
    }
    html += '></td>';
    html += '</tr>';
    return html;
}

function DisplayCameraVideoParam(v) {
    //console.log(v);
    if (v.items && v.items.length > 0) {
        _items = v.items;
        // Delete all rows in the Video Settings and Camera Controls tables
        $(".ctrlTable tr").remove();

        var tab1 = $("#tab1"), tab2 = $("#tab2");
        for (var i = 0; i < v.items.length; i++) {
            var item = v.items[i];
            var tr = buildTr(item);
            if (item.id == 6 || item.id == 105) {
                if (item.capsFlag == 0) {
                    $("#chk" + item.id).val(0).attr("disabled", "true");
                    $("#lb" + item.id).text(item.name).attr("disabled", "true");
                } else {
                    $("#chk" + item.id).val(item.val).removeAttr("disabled");
                    $("#lb" + item.id).text(item.name).removeAttr("disabled");
                }
            } else if (item.id < 100) {
                tab1.append(tr);
            } else {
                tab2.append(tr);
            }
        }
        $("#powerFreq").val(v.powerFreq);

        $(".valAdj").change(function(){
            var tr = this.parentNode.parentNode;
            var pid = parseInt(tr.getAttribute("pid"));
            var val = parseInt(this.value);
            $(tr).find(".valDisp").val(val);
            SendCameraVideoParam([{id: pid, val: val, flag: 2}]); // 2手动
        });
        $(".autoChk").change(function(){
            var tr = this.parentNode.parentNode;
            var pid = parseInt(tr.getAttribute("pid"));
            var objRange = $(tr).find(".valAdj");
            var val = parseInt(objRange.val());
            if (this.checked) {
                $(tr).find(".valAdj").attr("disabled", "true");
            } else {
                $(tr).find(".valAdj").removeAttr("disabled");
            }
            SendCameraVideoParam([{id: pid, val: val, flag: (this.checked ? 1 : 2)}]); // flag: 1:Auto，2:Manual
        });
    }
}

function restoreDefault(v) {
    if (_items && v > 0) {
        var ary = [];
        for (var i = 0; i < _items.length; i++) {
            var item = _items[i];
            if (item.capsFlag > 0 && (v == 1 && item.id < 100 || v > 1 && item.id >= 100)) {
                var objTr = $("tr[pid=" + item.id + "]");
                var autoCheked = objTr.find(".autoChk").prop("checked");
                objTr.find(".valAdj").val(item.defVal);
                objTr.find(".valDisp").val(item.defVal);
                ary.push({id: item.id, val: item.defVal, flag: (autoCheked ? 1 : 2)});
            }
        }
        SendCameraVideoParam(ary);
    }
}

function SendCameraVideoParam(ary) {
    SendJson({func:"CameraVideoParam", reqId: new Date().getTime(), devNum: _devNum, items: ary});
}

$(function () {
    // Obtain the list of camera devices
    GetCameraInfo();
})

function cameraCapture_Fun() {
    SendJson({func:"CameraCapture", reqId: new Date().getTime(), devNum: _devNum, mode: "path"});
}

function switchCameraInfo_Fun() {
    var reqId = new Date().getTime();
    if (_devNum >= 0) {
        SendJson({func:"CloseCamera", reqId: reqId, devNum: _devNum});
        reqId++;
    }

    _devNum = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
    SendJson({func:"OpenCamera", reqId: reqId, devNum: _devNum, mediaNum:0, resolutionNum:0, fps:5});
    reqId++;

    // Get preview video
    SendJson({func:"GetCameraVideoBuff", reqId: reqId, devNum: _devNum, enable: true});
    reqId++;

    // Read the camera video Settings of the camera
    SendJson({func:"CameraVideoParam", reqId: reqId, devNum: _devNum});
}

function onChangePowerFreq() {
    SendJson({func:"CameraVideoParam", reqId: new Date().getTime(), devNum: _devNum, powerFreq: parseInt($("#powerFreq").val())});
}

// Updated the device name drop-down list
function displayCamera() {
    var obj = document.getElementById("cameraInfo");
    var lastSelectedIndex = obj.options.selectedIndex;
    obj.options.length = 0;
    for (var i = 0; i < _devInfos.length; i++) {
        obj.options.add(new Option(_devInfos[i].devName, _devInfos[i].id));
    }
    if (lastSelectedIndex >= 0 && lastSelectedIndex < _devInfos.length) {
        obj.options.selectedIndex = lastSelectedIndex;
    }
}

function DisplayCaptureImg(v) {
    if (v.imagePath) {
        for (var i = 0; i < v.imagePath.length; i++) {
            displayFile(v.imagePath[i]);
        }
        audioCapture.play();
    }
}

var _path = null;
var _th = '<tr><th>File name</th><th>Operation</th></tr>';
var _tdOpration = '<td><a onclick="vf()">View</a> <a onclick="df()">Delete</a></td>';
function displayFile(file) {
    var pos = file.lastIndexOf('\\');
    if (_path == null) {
        _path = file.substring(0, pos);
    }
    var html = '<tr><td>' + file.substring(pos + 1) + '</td>' + _tdOpration + '</tr>';
    $("#tbFile").append(html);
}

// Clear the image display list
function clearImageFile() {
    $("#image_data").empty();
    $("#tbFile").empty().append(_th);
}

function convertToFile_Fun() {
    var reqId = new Date().getTime();
    var fileArray = new Array();
    var trs = $('#tbFile tr');
    for (i = 1; i < trs.length; i++) {
        fileArray.push(_path + '\\' + trs[i].children[0].innerText);
    }
    var lang = $("#languages").find("option:selected").text();
    var extName = $("#extNames").find("option:selected").text();
    SendJson({func: 'FileOCR', reqId: reqId, language: lang, extName: extName, DetectTextOrientation: false, imagePath:fileArray});
}

function mergePDF() {
    var reqId = new Date().getTime();
    var fileArray = new Array();
    var trs = $('#tbFile tr');
    for (i = 1; i < trs.length; i++) {
        fileArray.push(_path + '\\' + trs[i].children[0].innerText);
    }
    // fileType: 0 PDF; 1-TIFF(暂不支持)
    SendJson({func: 'MergeFile', reqId: reqId, fileType: 0, imagePath: fileArray, mode: 'path;base64'});
}

function vf() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    var file = _path + '\\' + $(target).parent().prev().text();

    // Convert the file to Base64 for display viewing
    SendJson({func:'FileToBase64', reqId:new Date().getTime(), filePath:file});
}

function DisplayFileView(v) {
    var img = document.getElementById("imgView");
    img.src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    $("#divView").css('display', '').css('width', '').css('height', '');
    if (img.height > $("#divView").innerHeight()) {
        $("#divView").css('height', img.height + 'px');
    }
    if (img.width > $("#divView").innerWidth()) {
        $("#divView").css('width', img.width + 'px');
    }
}

// Delete the photo file
function df() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    $(target).parent().parent().remove();
}

// Turn off picture viewing
function closeView() {
    $("#divView").css('display', 'none');
    document.getElementById("imgView").src = "";
}
