var _url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
var _connected = false;
var _devNum = -1;

Date.prototype.toLocaleString = function () {
    var y = this.getFullYear(), m = this.getMonth() + 1, d = this.getDate(), h = this.getHours(),
        mi = this.getMinutes(), s = this.getSeconds(), ms = this.getMilliseconds();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d) + ' ' + (h < 10 ? ('0' + h) : h) + ':' + (mi < 10 ? ('0' + mi) : mi) + ":" + (s < 10 ? ('0' + s) : s) + '.' + ((ms + 1000) + "").substr(1, 3);
};

/**
 * init webSocket connect
 */
function ConnectServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _connected = true;
        callback(value);
    }
    _ws.onclose = function (e) {
        _connected = false;
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            onTxtMessage(e.data);
        } else {
            onBinMessage(e);
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn);
    };
}

/**
 * Sends information to the server
 */
function SendTxt(jsonStr) {
    _connected ? _ws.send(jsonStr) : ConnectServer(SendTxt, jsonStr)
}

function SendJson(json) {
    _connected ? _ws.send(JSON.stringify(json)) : ConnectServer(SendJson, json)
}

/**
 * rocess server binary messages
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

/**
 * Process received server text messages
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the execution is successful. If other values are returned, the execution is abnormal.
        // 9: The device is in use. Ignore this exception when you start the camera
        console.log(jsonObj.func, jsonObj.errorMsg);
        alert(jsonObj.func + "\r\n" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                break;
            case "RecogBarCode":
                DisplayBarCodeResult(jsonObj);
                break;
            case "CloseCamera":
                document.getElementById("video").src = "";
                break;
            case "Notify":
                // receipt of notification
                if (jsonObj.event == "OnUsbKeyPress") {
                    // Received notification of USB button press
                    // {"func":"Notify","event":"OnUsbKeyPress","reqId":1636099207839.0,"result":0,"time":"1636099214"}
                    // DisplayUsbKeyPress(jsonObj);
                } else if (jsonObj.event == "OnDeviceChanged") {
                    // The format of the device is as follows:
                    // {"func":"Notify","event":"OnDeviceChanged","devName":"\\\\?\\USB#VID_0C45&PID_6366...","func":"OnDeviceChanged","result":0,"type":1}
                    // type: 1 Device insertion; 2 The device is removed
                    GetCameraInfo();
                }
                break;
            case "CameraAutoCapture":
                DisplayStatus(jsonObj);
                break;
            default:
                console.log(msg);
                break;
        }
    }
}

// The bar code identification result is displayed
function DisplayBarCodeResult(jsonObj) {
    if (jsonObj.codeInfos) {
        var a = jsonObj.codeInfos;
        var s = Lang.cam.res + "\r\n";
        for (var i = 0; i < a.length; i++) {
            s += Lang.cam.content + a[i].codeNum + "\r\n";
            s += Lang.cam.type + a[i].codeType + "\r\n";
            s += "--------\r\n"
        }
        $("#resultArea").val(s);
    } else {
        $("#resultArea").val(Lang.cam.noRrecognized);
    }
}

// Display video
function DisplayVideo(v) {
    document.getElementById("video").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
}

function DisplayDevInfo(devInfos) {
    var obj = document.getElementById("cameraInfo");
    var lastSelectedIndex = obj.options.selectedIndex;
    obj.options.length = 0;
    for (var i = 0; i < devInfos.length; i++) {
        obj.options.add(new Option(devInfos[i].devName, devInfos[i].id));
    }
    if (lastSelectedIndex >= 0 && lastSelectedIndex < devInfos.length) {
        obj.options.selectedIndex = lastSelectedIndex;
    }
}

function DisplayCaptureImg(json) {
    if (json.imgBase64 && json.imgBase64.length > 0) {
        document.getElementById("imgCapture").src = "data:" + json.mime + ";base64," + json.imgBase64[0];
        document.getElementById("captureInfo").innerText = "自动拍照成功, " + new Date().toLocaleString();
        progress0.value = progress0.max;
        audioCapture.play();
    }
}

function onChangeFile() {
    var files = document.getElementById("imgFile").files;
    if (files.length > 0) {
        fileToBase64(files[0], "display");
    }
}

function onChangeCamera() {
    closeCamera();
    openCamera();
}

function onChangeType() {
    var t = $("input[name='srcType']:checked").val();
    switch (t) {
        case "1":
            $("#imgPath").removeAttr("disabled");
            $("#imgFile").attr("disabled","disabled");
            $("#cameraInfo").attr("disabled","disabled");
            closeCamera();
            break;
        case "2":
            $("#imgPath").attr("disabled","disabled");
            $("#imgFile").removeAttr("disabled");
            $("#cameraInfo").attr("disabled","disabled");
            closeCamera();
            break;
        case "3":
            $("#imgPath").attr("disabled","disabled");
            $("#imgFile").attr("disabled","disabled");
            $("#cameraInfo").removeAttr("disabled");
            openCamera();
            break;
    }
    document.getElementById("video").src = "";
}

function closeCamera() {
    if (_devNum >= 0) {
        SendJson({func: "CloseCamera", reqId: new Date().getTime(), devNum: _devNum});
        _devNum = -1;
    }
}

function openCamera() {
    if (_devNum < 0) {
        _devNum = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
        var reqId = new Date().getTime();
        var cmd = {func: "OpenCamera",reqId: reqId, devNum: _devNum, mediaNum: 0, resolutionNum: 0, fps: 5};
        SendJson(cmd);
        reqId++;

        // Set single figure cutting (cutting mode: 0- no cutting; 1- Single figure cutting; 2- Multi-picture cutting)
        SendJson({func: "SetCameraImageInfo", reqId: reqId, devNum: _devNum, cropType: 1});
        reqId++;

        // Get preview video
        SendJson({func: "GetCameraVideoBuff", reqId: reqId, devNum: _devNum, enable: true});
    }
}

// Get Device info
function GetCameraInfo() {
    SendJson({func: "GetCameraInfo", reqId: new Date().getTime()});
}

function DisplayStatus(v) {
    if (v.enable) {
        $('#status').text(Lang.autoCapture.started + (v.detectionType == 1 ? Lang.autoCapture.timeLapse : Lang.autoCapture.auto));
    } else {
        $('#status').text(Lang.autoCapture.stopped);
    }
}

function fileToBase64(file, action) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function (e) {
        console.info(reader.result);
        if (reader.result) {
            if (action == "display") {
                document.getElementById("video").src = reader.result;
            } else {
                var reqId = new Date().getTime();
                var pos = reader.result.indexOf(";base64,");
                var imgBase64 = reader.result.substr(pos + 8);
                var mime = reader.result.substring(5, pos);
                SendJson({func: "RecogBarCode", reqId: reqId, imageBase64: imgBase64, mime: mime});
            }
        }
    }

    var binary = '';
    var size = file.size;
    var blob = new Blob([file]);
    var len = blob.byteLength;
    for (var i = 0; i < size; i++) {
        binary += String.fromCharCode(blob[i]);
    }
    return window.btoa(binary);
}

function recogBarCode() {
    var reqId = new Date().getTime();
    var t = $("input[name='srcType']:checked").val();
    switch (t) {
        case "1":
            var path = $("#imgPath").val();
            if (path.length == 0) {
                alert(Lang.barcode.promptInputPath);
            } else {
                SendJson({func: "RecogBarCode", reqId: reqId, imagePath: path});
            }
            break;
        case "2":
            var files = document.getElementById("imgFile").files;
            if (files.length == 0) {
                alert(Lang.barcode.promptSelectFile);
                alert("");
            } else {
                fileToBase64(files[0], "send");
            }
            break;
        case "3":
            SendJson({func: "RecogBarCode", reqId: reqId, devNum: _devNum});
            break;
    }
}

$(function () {
    // Obtain the list of camera devices
    GetCameraInfo();
});
