var _devInfos = null;
var _devNum = -1;
var _myWs = new MzWsConnect(location.port, onTxtMessage, onError, ClearOutputInfo);

// Handling connection errors
function onError(e, msg) {
    displayOutputInfo(3, msg);
}

/**
 * Process server binary messages
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

function ClearOutputInfo() {
    displayOutputInfo(0);
}

/**
 * Process the received server text message
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the command is successfully executed. Other values are abnormal.
        // 9 indicates that this exception is ignored when the camera is in use
        console.log(jsonObj.func, jsonObj.errorMsg);
        displayOutputInfo(3, jsonObj.func + "<br>" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj);
                break;
            case "OpenCamera":
                // After the document camera is successfully opened, set 'Remove Finger' 'Single Image Crop' to obtain the video of the document camera
                var reqId = new Date().getTime();
                _myWs.SendJson({func: 'SetCameraImageInfo', devNum:_devNum, removalForeign:1, cropType:1, reqId:reqId++});
                _myWs.SendJson({func: "GetCameraVideoBuff", devNum: _devNum, enable: true, reqId: reqId++});
                break;
            case "CloseCamera":
                displayOutputInfo(2, "The camera is close successfully.");
                break;
            case "GetCameraVideoBuff":
                // Get the video from the camera, display
                DisplayVideo(jsonObj);
                break;
            case "CameraCapture":
                displayFile(jsonObj);
                break;
            case "FileToBase64":
                DisplayFileView(jsonObj);
                break;
            case 'UploadHttpFile':
                displayOutputInfo(2, "Http upload succeed.<br>responseCode: " + jsonObj.responseCode + "<br>response: " + jsonObj.response);
                break;
            case 'UploadFtpFile':
                displayOutputInfo(2, "FTP upload succeed.<br>" + jsonObj.remoteFilePath);
            default:
                console.log(msg);
                if (jsonObj.result != 0) {
                    displayOutputInfo(3, jsonObj.func + "<br>" + jsonObj.errorMsg);
                }
                break;
        }
    }
}

function DisplayDevInfo(v) {
    if (v.result == 0) {
        _devInfos = v.devInfo;
        if (_devInfos.length == 0) {
            displayOutputInfo(3, "When you get device information, the devInfos returned is empty.");
            return;
        }

        displayCamera();
        displayMediaType();
        displayResolution();
        switchCamera();
    } else {
        displayOutputInfo(3, v.func + "<br>" + v.errorMsg);
    }
}

// Refresh the device name drop-down list
function displayCamera() {
    var obj = document.getElementById("cameraInfo");
    var selectedIndex = -1;
    obj.options.length = 0;
    for (var i = 0; i < _devInfos.length; i++) {
        obj.options.add(new Option(_devInfos[i].devName, _devInfos[i].id));
        if (selectedIndex < 0 && _devInfos[i].camMode == 0) {
            // Open the first master Camera
            selectedIndex = i;
        }
    }
    obj.options.selectedIndex = selectedIndex;
}

// Refresh the video format drop-down list
function displayMediaType() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var mediaTypes = _devInfos[index1].mediaTypes;
    var obj = document.getElementById("mediaType");
    obj.options.length = 0;
    for (var i = 0; i < mediaTypes.length; ++i) {
        obj.options.add(new Option(mediaTypes[i].mediaType, i));
    }
    obj.options.selectedIndex = 0;
}

// Refresh the display resolution drop-down list
function displayResolution() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var index2 = document.getElementById("mediaType").options.selectedIndex;
    var obj = document.getElementById("resolution");
    var resolutions = _devInfos[index1].mediaTypes[index2].resolutions;
    var selectedIndex = -1;
    // clear resolution:select data
    obj.options.length = 0;
    for (var i = 0; i < resolutions.length; i++) {
        obj.options.add(new Option(resolutions[i], i));
        if (selectedIndex < 0 && Between(resolutions[i].split('x')[0], 2000, 3000)) {
            // Find the first resolution between 2000 and 3000
            selectedIndex = i;
        }
    }
    obj.options.selectedIndex = 0;
}

function switchCamera() {
    var reqId = new Date().getTime();
    if (_devNum >= 0) {
        // Turn off the previous camera
        _myWs.SendJson({func:"CloseCamera", reqId: reqId++, devNum: _devNum});
    }

    // Turning on the Camera
    _devNum = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
    var mediaType = parseInt($("#mediaType").val());
    var resolutionNum = parseInt($("#resolution").val());
    _myWs.SendJson({func:"OpenCamera", devNum:_devNum, mediaNum:mediaType, resolutionNum:resolutionNum, reqId:reqId});
}


// Open (1) or close (0) the portrait camera, according to the text on the button when there is no parameter
function OpenCloseCamera(val) {
    var open = val != 0;
    var now = new Date().getTime();
    if (_devNum >= 0) {
        // Turn it off before you turn it on. Stop the video before you turn it off
        _myWs.SendJson({func:"GetCameraVideoBuff", devNum:_devNum, enable:false, reqId: now++});
        _myWs.SendJson({func:"CameraAutoCapture", devNum:_devNum, enable:false, reqId: now++});
        _myWs.SendJson({func:"CloseCamera", devNum:_devNum, reqId: now++});
        _devNum = -1;
    }
    if (open) {
        _devNum = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
        var mediaType = parseInt($("#mediaType").val());
        var resolutionNum = parseInt($("#resolution").val());
        _myWs.SendJson({func:"OpenCamera", to:1, devNum:_devNum, mediaNum:mediaType, resolutionNum:resolutionNum, reqId: now++});
    }
}

function ReOpenCamera() {
    // Restart the document camera on the multifunction terminal
    OpenCloseCamera(1);
}

function Between(v, maxV, minV) {
    return v <= maxV && v >= minV;
}

var _lastVideoLeft = 0, _lastVideoTop = 0, _lastVideoW = 0, _lastVideoH = 0;
var _w = 0, _h = 0, _wp = 0, _hp = 0;
function DisplayVideo(v) {
    if (v.imgBase64Str) {
        if (_w != v.width || _h != v.height) {
            // Set the width and height of the video display (640px)
            _wp = 640, _hp = _wp * v.height / v.width, _w = v.width, _h = v.height;
            $("#video").css('width', _wp + 'px').css('height', _hp + 'px');
        }
        // show video
        document.getElementById("video").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    }
}

var _path = null;
var _th = '<tr><th>File Name</th><th>Operation</th></tr>';
var _tdOpration = '<td><a onclick="vf()">View</a> <a onclick="df()">Del</a></td>';
function displayFile(v) {
    for (var i = 0; i < v.imagePath.length; ++i) {
        var file = v.imagePath[i];
        var pos = file.lastIndexOf('\\');
        if (_path == null) {
            _path = file.substring(0, pos);
        }
        var html = '<tr><td>' + file.substring(pos + 1) + '</td>' + _tdOpration + '</tr>';
        $("#tabBody").append(html);
    }
}

function DisplayImg(mime, imgBase64Str) {
    document.getElementById("imgView").src = "data:" + mime + ";base64," + imgBase64Str;
}

function vf() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    var file = _path + '\\' + $(target).parent().prev().text();

    // Convert the file to base64 for display viewing
    _myWs.SendJson({func:'FileToBase64', filePath:file, reqId:new Date().getTime()});
}

function DisplayFileView(v) {
    document.getElementById("imgView").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    $('#myModal').modal();
}

// Deleting a file
function df() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    $(target).parent().parent().remove();
}

// Delete all files
function clearImageFile() {
    $("#tabBody").empty();
}

function OpenFileByPath(filePath) {
    if (location.protocol.substr(0,4) == 'http') {
        var pos = filePath.lastIndexOf('\\');
        pos = filePath.lastIndexOf('\\', pos - 1);
        var url = location.origin + '/tmp/' + filePath.substr(pos + 1).replace('\\','/');
        window.open(url);
    }
}

function displayOutputInfo(disp, s, add) {
    if (disp > 0 && disp < 4) {
        var ary = ["info", "success", "warning"];
        var h = "<b>" + s + "</b>";
        if (add) {
            var h0 = $('#outInfo').html();
            if (h0.length > 0) {
                h = h0 + "<br>" + h;
            }
        }
        $('#outInfo').css("display", "").attr("class", "col-md-12 alert alert-" + ary[disp - 1]).html(h);
    } else {
        $('#outInfo').css("display", "none").html("");
    }
}

// The file list of the photos in the file, the {File1: "11. JPG", File2: "22. JPG",... } such map object
// When uploading files through HTTP, "File1" represents the name of form-data. The HTTP code is: Content-Disposition: form-data; name="File1"; filename="11.jpg"
function getImgFileMap() {
    var fileMap = {};
    var trs = $('#tabBody tr');
    for (i = 0; i < trs.length; i++) {
        fileMap["File" + (i + 1)] = _path + '\\' + trs[i].children[0].innerText;
    }
    return fileMap;
}

function setCropType() {
    var cropType = parseInt($("#crop_type").val());
    _myWs.SendJson({func:'SetCameraImageInfo', reqId:new Date().getTime(), devNum:_devNum, cropType:cropType});
}

function DoCapture() {
    _myWs.SendJson({func: 'CameraCapture', reqId: new Date().getTime(), devNum: _devNum, mode: 'path'});
}

function httpUpload() {
    var url1 = $("#url1").val();
    var cmd = {func: 'UploadHttpFile', url:url1, files:getImgFileMap(), reqId: new Date().getTime()};
    var cookieVal1 = $("#cookieVal1").val();
    if (cookieVal1.length > 0) {
        cmd.cookie = cookieVal1;
    }

    cmd.params = {};
    var PNs = document.getElementsByName("ParamN");
    var PVs = document.getElementsByName("ParamV");
    for (var i = 0; i < PNs.length; i++) {
        var pn = PNs[i].value;
        var pv = PVs[i].value;
        if (pn.length > 0) {
            cmd.params[pn] = pv;
        }
    }
    // cmd.params = {param1: "val1", param2: "val2", param3: "val3"};
    _myWs.SendJson(cmd);
    displayOutputInfo(1, '文件HTTP上传中...');
}

function addP() {
    $("#spanParam").append('<label>Name:</label><input name="ParamN" type="text" class="PN"/><label>Value:</label><input name="ParamV" type="text" class="PV"/><a href="#" onclick="delP()">X</a><br>');
}

function delP() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    $(target).prev().remove();
    $(target).prev().remove();
    $(target).prev().remove();
    $(target).prev().remove();
    $(target).next().remove();
    $(target).remove();
}

function ftpUpload() {
    var userName = $("#userName").val();
    var passWd = $("#passWd").val();
    var url1 = $("#url2").val();
    if (url1.charAt(url1.length-1) !== '/') {
        url1 += '/';
    }
    var reqId = new Date().getTime();

    var trs = $('#tabBody tr');
    for (i = 0; i < trs.length; i++) {
        var fileName = trs[i].children[0].innerText;
        _myWs.SendJson({func: 'UploadFtpFile', remoteFilePath:url1 + fileName, localFilePath:_path + '\\' + fileName,
            userName: userName, password: passWd, reqId: reqId++});
        displayOutputInfo(1, 'File FTP uploading, ' + fileName + ' ...');
    }
}

$(function () {
    var now = new Date().getTime();
    // Obtain the device information about the camera
    _myWs.SendJson({func:"GetCameraInfo", reqId:now});
});

$(window).unload(function(){
    // Before closing the page, close the camera
    if (_devNum >= 0) {
        OpenCloseCamera(0);
    }
});
