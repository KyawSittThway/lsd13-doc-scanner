var _url = ((location.protocol === "https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
var _connected = false;
var _devInfos = null;
var _deviceNumber = -1;
var _supportOcr = 0;
var _cropBox = null;
/**
 * init webSocket connection
 */
function ConnectServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _connected = true;
        callback(value);
    }
    _ws.onclose = function (e) {
        _connected = false;
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            onTxtMessage(e.data);
        } else {
            onBinMessage(e);
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn)
    };
}

/**
 * Sends information to the server
 */
function sendText(jsonStr) {
    _connected ? _ws.send(jsonStr) : ConnectServer(sendText, jsonStr)
}
function SendJson(json) {
    _connected ? _ws.send(JSON.stringify(json)) : ConnectServer(SendJson, json)
}

/**
 * Process server binary messages
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

/**
 * Process received server text messages
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the execution is successful. If other values are returned, the execution is abnormal. 
        //  9: The device is in use. Ignore this exception when you start the camera
        console.log(jsonObj.func, jsonObj.errorMsg);
        alert(jsonObj.func + "\r\n" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                // Get OCR support information (list of languages)
                GetOcrInfo();
                // The USB button is disabled by default
                OnExtButtonChange();
                break;
            case "GetOcrSupportInfo":
                DisplayOcrSupportInfo(jsonObj);
                break;
            case "CameraCapture":
                for (var i = 0; i < jsonObj.imgBase64.length; i++) {
                    DisplayImg(jsonObj.mime, jsonObj.imgBase64[i]);
                }
                break;
            case "RecogBarCode":
                DisplayBarCodeResult(jsonObj);
                break;
            case "Notify":
                // receipt notify
                if (jsonObj.event == "OnUsbKeyPress") {
                    // Received notification of USB button press
                    // {"func":"Notify","event":"OnUsbKeyPress","reqId":1636099207839.0,"result":0,"time":"1636099214"}
                    DisplayUsbKeyPress(jsonObj);
                } else if (jsonObj.event == "OnDeviceChanged") {
                    // The format of the device change is as follows:
                    // {"func":"Notify","event":"OnDeviceChanged","devName":"\\\\?\\USB#VID_0C45&PID_6366...","func":"OnDeviceChanged","result":0,"type":1}
                    // type: 1-Device insertion; 2-Device removed
                    GetCameraInfo();
                }
                break;
            default:
                console.log(msg);
                break;
        }
    }
}

// Obtain camera device info
function GetCameraInfo() {
    sendText('{"func":"GetCameraInfo","reqId":"' + new Date().getTime() + '"}');
}

// Obtain OCR support information
function GetOcrInfo() {
    sendText('{"func":"GetOcrSupportInfo","reqId":"' + new Date().getTime() + '"}');
}

// The bar code identification result is displayed
function DisplayBarCodeResult(jsonObj) {
    if (jsonObj.codeInfos) {
        var a = jsonObj.codeInfos;
        var s = Lang.cam.res + "\r\n";
        for (var i = 0; i < a.length; i++) {
            s += Lang.cam.content + a[i].codeNum + "\r\n";
            s += Lang.cam.type + a[i].codeType + "\r\n";
            s += "--------\r\n"
        }
        $("#resultArea").val(s);
    } else {
        $("#resultArea").val(Lang.cam.noRrecognized);
    }
}

function DisplayUsbKeyPress(v) {
    var str = $("#resultArea").val();
    $("#resultArea").val(Lang.MSG.usbKeyPress + " time:" + v.time + "\r\n" + str);
}

var _w = 0, _h = 0;
function DisplayVideo(v) {
    if (v.imgBase64Str) {
        if (_w != v.width || _h != v.height) {
            // Sets the width and height of the video control (Width control is 640px)
            if (v.width > v.height) {
                $("#pic").css('width', '640px').css('height', 640 * v.height / v.width + 'px');
            } else {
                $("#pic").css('height', '640px').css('width', 640 * v.width / v.height + 'px');
            }
            _w = v.width, _h = v.height;
        }
        // show video
        document.getElementById("pic").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    }
}

function DisplayDevInfo(devInfos) {
    _devInfos = devInfos;
    if (_devInfos.length == 0) {
        alert(Lang.cam.doc);
        return;
    }
    displayCamera();
    displayMediaType();
    displayResolution();

    switchCameraInfo_Fun();
}

function DisplayOcrSupportInfo(v) {
    if (v.languages) {
        // Orc support for 'auto text orientation'
        _supportOcr = 1;
        document.getElementById('opOcrDirection').disabled = false;

        var obj = document.getElementById("language");
        obj.options.length = 0;
        for (var i = 0; i < v.languages.length; i++) {
            // The language ID starts at 1
            obj.options.add(new Option(v.languages[i], i + 1));
        }
        // Default: 'Simplified Chinese +English'
        obj.selectedIndex = 98;
    }
}

$(function () {
    //Obtain the list of camera devices
    GetCameraInfo();

    window.onresize = onWinResize;
    onWinResize();

    _cropBox = new CropBox("pic");
})

function onWinResize() {
    // Initialize the height of the picture list
    $("#right_div").css('height', (document.body.clientHeight - 30) + 'px');
    // Initialize 'result display' height
    var p = $("#resultArea").offset();
    $("#resultArea").css('height', (document.body.clientHeight - p.top - 16) + 'px');

    var cropType = parseInt($("#crop_type").val());
    if (cropType == 3) {
        showMiddleLine_Fun(cropType);
    }
    if (_cropBox) {
        _cropBox.adjustPos();
    }
}

// Onchange event of device information 
function switchCameraInfo_Fun() {
    var reqId = new Date().getTime();
    if (_deviceNumber >= 0) {
        // close the old cameras
        sendText('{"func":"CloseCamera","reqId":"' + reqId + '","devNum":' + _deviceNumber + '}');
    }

    // Refresh the display video format drop-down list
    displayMediaType();

    // Refresh the display resolution drop-down list
    displayResolution();

    // Open camera
    _deviceNumber = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
    reqId++;
    var cmd = '{"func":"OpenCamera","reqId":"' + reqId + '","devNum":' + _deviceNumber + ',"mediaNum":0,"resolutionNum":0,"fps":5}';
    sendText(cmd);

    // Get preview video
    reqId++;
    cmd = '{"func":"GetCameraVideoBuff","reqId":"' + reqId + '","devNum":' + _deviceNumber + ',"enable":"true"}';
    sendText(cmd);
}

function cameraCapture_Fun() {
    // Set image algorithm
    setCameraImageInfo_Fun();
    // Camera capture
    var reqId = new Date().getTime();
    var cmd = JSON.stringify({ func: 'CameraCapture', reqId: reqId, devNum: _deviceNumber, mode: 'base64' });
    sendText(cmd);
}

function recogBarCode_Fun() {
    $("#resultArea").val(Lang.cam.bar);
    var reqId = new Date().getTime();
    var cmd = JSON.stringify({ 'func': 'RecogBarCode', 'reqId': reqId, 'devNum': _deviceNumber });
    sendText(cmd);
}

// Updated the device name drop-down list
function displayCamera() {
    var obj = document.getElementById("cameraInfo");
    var lastSelectedIndex = obj.options.selectedIndex;
    obj.options.length = 0;
    for (var i = 0; i < _devInfos.length; i++) {
        obj.options.add(new Option(_devInfos[i].devName, _devInfos[i].id));
    }
    if (lastSelectedIndex >= 0 && lastSelectedIndex < _devInfos.length) {
        obj.options.selectedIndex = lastSelectedIndex;
    }
}

// Refresh the display video format drop-down list
function displayMediaType() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var mediaTypes = _devInfos[index1].mediaTypes;
    var obj = document.getElementById("mediaType");
    obj.options.length = 0;
    for (var i = 0; i < mediaTypes.length; ++i) {
        obj.options.add(new Option(mediaTypes[i].mediaType, i));
    }
}

// Refresh the display resolution drop-down list
function displayResolution() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var index2 = document.getElementById("mediaType").options.selectedIndex;
    var obj = document.getElementById("resolution");
    var resolutions = _devInfos[index1].mediaTypes[index2].resolutions;

    obj.options.length = 0;
    for (var i = 0; i < resolutions.length; i++) {
        obj.options.add(new Option(resolutions[i], i));
    }
}

//Setting Device Information
function setCameraInfo_Fun() {
    //Resolution value
    var resolutionNum = $("#resolution").val() == null ? 0 : parseInt($("#resolution").val());
    var mediaNum = $("#mediaType").val() == null ? 0 : parseInt($("#mediaType").val());
    var reqId = new Date().getTime();
    var cmd = JSON.stringify({ 'func': 'SetCameraInfo', 'reqId': reqId, 'devNum': _deviceNumber, 'mediaNum': mediaNum, 'resolutionNum': resolutionNum });
    sendText(cmd);
}

function setCameraImageInfo_Fun() {
    var cropType = parseInt($("#crop_type").val());
    //Surface correction requires a green line in the middle
    showMiddleLine_Fun(cropType);
    // Controls the display of custom cutting frames
    allowCropBox(cropType);
    var imageType = parseInt($("#image_type").val());
    var fillBorderType = parseInt($("#fill_border").val());
    var removalForeign = parseInt($("#foreign_remove").val());
    var rotateAngle = parseInt($("#rotateType").val());
    var langId = parseInt($("#language").val());
    var reqId = new Date().getTime();

    var cropBoxArray = null;
    if (cropType == 9) {
        cropBoxArray = _cropBox.getCropBoxArray();
        if (cropBoxArray.length < 6) {
            // If custom cutting is set, but no cutting frame is drawn, press "No cutting"
            cropType = 0;
            cropBoxArray = null;
        }
    }

    var cmd = {
        func: 'SetCameraImageInfo', reqId: reqId, devNum: _deviceNumber, cropType: cropType,
        imageType: imageType, fillBorderType: fillBorderType, removalForeign: removalForeign, rotate: rotateAngle,
        textOrientationLangId: langId
    };
    if (cropBoxArray) {
        cmd.posArray = cropBoxArray;
    }

    sendText(JSON.stringify(cmd));
}

function OnRotateChange() {
    if (_supportOcr) {
        $("#language").parent().css('display', document.getElementById("rotateType").selectedIndex == 4 ? '' : 'none');
        var p = $("#resultArea").offset();
        $("#resultArea").css('height', (document.body.clientHeight - p.top - 10) + 'px');
    }
    setCameraImageInfo_Fun();
}

function OnExtButtonChange() {
    var cbFun = $("#selExtButton").val();
    var cmd = { func: 'ExternalButton', reqId: new Date().getTime() };
    if (cbFun == "None") {
        cmd.enable = false;
    } else {
        cmd.enable = true;
        cmd.callbackFunc = cbFun;
        // Added additional attributes for each Angle command
        switch (cbFun) {
            //Notify,CameraCapture,CameraCaptureBook,RecogBarCode
            case 'CameraCapture':
                cmd.devNum = _deviceNumber;
                cmd.mode = 'base64';
                break;
            case 'RecogBarCode':
                cmd.devNum = _deviceNumber;
                break;
        }
    }
    SendJson(cmd);
}

// Surface correction requires a green line in the middle
function showMiddleLine_Fun(cropType) {
    if (cropType == 3) {
        var obj = $("#pic");
        var p = obj.offset(), w = obj.width(), h = obj.height();
        $("#vertical-line").css('top', p.top + 'px').css('left', (p.left + w / 2 - 2) + 'px').css('height', h + 'px').css('display', '');
        $("#vertical-line").show();
    } else {
        $("#vertical-line").hide();
    }
}

// Controls the display of custom cutting frames
function allowCropBox(cropType) {
    if (cropType == 9) {
        _cropBox.enable();
    } else {
        _cropBox.disable();
    }
}

// Add an image item
function DisplayImg(mime, imgBase64Str) {
    var h = '<div>';
    h += '<a href="javascript:void(0)" onclick="delP()" class="img-del"></a>';
    h += '<img width="360" src="' + 'data:' + mime + ';base64,' + imgBase64Str + '">';
    h += '</div>';
    $("#image_data").prepend(h);
}

// delete image file
function delP() {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    $(target).parent().remove();
}

// Clear the image display list
function clearImageFile() {
    $("#image_data").empty();
}
