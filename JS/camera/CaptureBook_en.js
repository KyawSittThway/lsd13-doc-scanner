var _url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
var _connected = false;
var _devInfos = null;
var _devNum = -1;
var _setRemovalForeign = false; //Is' Finger removal 'set?

/**
 * init webSocket connect
 */
function ConnectServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _connected = true;
        callback(value);
    }
    _ws.onclose = function (e) {
        _connected = false;
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            onTxtMessage(e.data);
        } else {
            onBinMessage(e);
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn);
    };
}

/**
 * Sends information to the server.
 */
function SendTxt(jsonStr) {
    _connected ? _ws.send(jsonStr) : ConnectServer(SendTxt, jsonStr)
}

/**
 * Process server binary messages.
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

/**
 * Process received server text messages.
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the execution is successful. If other values are returned, the execution is abnormal.9: The device is in use. Ignore this exception when you start the camera.
        console.log(jsonObj.func, jsonObj.errorMsg);
        alert(jsonObj.func + "\r\n" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                break;
            case "CameraCaptureBook":
                DisplayCaptureBook(jsonObj);
                break;
            case "FileToBase64":
                DisplayFileView(jsonObj);
                break;
            case "MergeFile":
                DisplayMergeFile(jsonObj);
                break;
            case "Notify":
                if (jsonObj.event == "OnUsbKeyPress") {
                    // 收到USB按键按下的通知
                    // {"func":"Notify","event":"OnUsbKeyPress","reqId":1636099207839.0,"result":0,"time":"1636099214"}
                    // DisplayUsbKeyPress(jsonObj);
                } else if (jsonObj.event == "OnDeviceChanged") {
                    // The device has changed. The format is as follows:
                    // {"func":"Notify","event":"OnDeviceChanged","devName":"\\\\?\\USB#VID_0C45&PID_6366...","func":"OnDeviceChanged","result":0,"type":1}
                    // type: 1 Device insertion;2 The device is removed.
                    GetCameraInfo();
                }
                break;
            default:
                console.log(msg);
                break;
        }
    }
}

// get device info.
function GetCameraInfo() {
    SendTxt('{"func":"GetCameraInfo","reqId":"' + new Date().getTime() + '"}');
}

//Displays the merged PDF file.
function DisplayMergeFile(jsonObj) {
    if (jsonObj.filePath) {
        alert(Lang.MSG.mergePdfOk + "\r\n" + jsonObj.filePath);
        if (location.protocol.substr(0,4) == 'http') {
            var pos = jsonObj.filePath.lastIndexOf('\\');
            pos = jsonObj.filePath.lastIndexOf('\\', pos - 1);
            var url = location.origin + '/tmp/' + jsonObj.filePath.substr(pos + 1).replace('\\','/');
            window.open(url);
        }
    }
}

var _lastVideoLeft = 0, _lastVideoTop = 0, _lastVideoW = 0, _lastVideoH = 0;
var _w = 0, _h = 0, _wp = 0, _hp = 0;
function DisplayVideo(v) {
    if (_w != v.width || _h != v.height) {
        //Sets the width and height of the video control. The height control is 484pix
        _hp = 484, _wp = _hp * v.width / v.height, _w = v.width, _h = v.height;
        $("#pic").css('width', _wp + 'px').css('height', _hp + 'px');
    }

    //show video.
    document.getElementById("pic").src = "data:" + v.mime + ";base64," + v.imgBase64Str;

    // Shows the green line in the middle, used to align the middle seam of the book.
    var p = $("#pic").offset();
    if (p.left != _lastVideoLeft || p.top != _lastVideoTop || _lastVideoW != _wp || _lastVideoH != _hp) {
        $("#vertical-line").css('top', p.top + 'px').css('left', (p.left + _wp/2 - 2) + 'px').css('height', _hp + 'px').css('display','');
        _lastVideoLeft = p.left, _lastVideoTop = p.top, _lastVideoW = _wp, _lastVideoH = _hp;
    }
}

function DisplayDevInfo(devInfos) {
    _devInfos = devInfos;
    if (_devInfos.length == 0) {
        alert(Lang.cam.doc);
        return;
    }
    //Init device info.
    displayCamera();
    displayMediaType();
    displayResolution();

    switchCameraInfo_Fun();
}

//init.
$(function () {
    //Get the list of camera devices.
    GetCameraInfo();
})

//Onchange event of device information.
function switchCameraInfo_Fun() {
    var reqId = new Date().getTime();
    if (_devNum >= 0) {
        //Close the old cameras.
        SendTxt('{"func":"CloseCamera","reqId":"' + reqId + '","devNum":' + _devNum + '}');
        reqId++;
    }

    //Refresh, Video compression, drop-down list.
    displayMediaType();

    //Refresh, resolution drop-down list.
    displayResolution();

    //Open camera.
    _devNum = $("#cameraInfo").val() == null ? 0 : parseInt($("#cameraInfo").val());
    var cmd = '{"func":"OpenCamera","reqId":"' + reqId + '","devNum":' + _devNum + ',"mediaNum":0,"resolutionNum":0,"fps":5}';
    SendTxt(cmd);
    reqId++;

    // Get preview video.
    cmd = '{"func":"GetCameraVideoBuff","reqId":"' + reqId + '","devNum":' + _devNum + ',"enable":"true"}';
    SendTxt(cmd);

    _setRemovalForeign = false;
}

//Refresh,device name,drop-down list.
function displayCamera() {
    var obj = document.getElementById("cameraInfo");
    var lastSelectedIndex = obj.options.selectedIndex;
    obj.options.length = 0;
    for (var i = 0; i < _devInfos.length; i++) {
        obj.options.add(new Option(_devInfos[i].devName, _devInfos[i].id));
    }
    if (lastSelectedIndex >= 0 && lastSelectedIndex < _devInfos.length) {
        obj.options.selectedIndex = lastSelectedIndex;
    }
}

//Refresh, Video compression, drop-down list.
function displayMediaType() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var mediaTypes = _devInfos[index1].mediaTypes;
    var obj = document.getElementById("mediaType");
    obj.options.length = 0;
    for (var i = 0; i < mediaTypes.length; ++i) {
        obj.options.add(new Option(mediaTypes[i].mediaType, i));
    }
}

//Refresh, resolution drop-down list.
function displayResolution() {
    var index1 = document.getElementById("cameraInfo").options.selectedIndex;
    var index2 = document.getElementById("mediaType").options.selectedIndex;
    var obj = document.getElementById("resolution");
    var resolutions = _devInfos[index1].mediaTypes[index2].resolutions;
    //Clear resolution:select data.
    obj.options.length = 0;
    for (var i = 0; i < resolutions.length; i++) {
        obj.options.add(new Option(resolutions[i], i));
    }
}

//Set device info.
function setCameraInfo_Fun() {
    //Resolution value
    var resolutionNum = $("#resolution").val() == null ? 0 : parseInt($("#resolution").val());
    var mediaNum = $("#mediaType").val() == null ? 0 : parseInt($("#mediaType").val());
    var reqId = new Date().getTime();
    var cmd = JSON.stringify({'func': 'SetCameraInfo', 'reqId':reqId, 'devNum':_devNum, 'mediaNum':mediaNum,'resolutionNum': resolutionNum});
    SendTxt(cmd);
}

function DisplayCaptureBook(json) {
    if (json.pageL) {
        displayFile(json.pageL);
        DisplayImg(json.mime, json.pageLBase64Str);
        document.getElementById("imgL").src = "data:" + json.mime + ";base64," + json.pageLBase64Str;
    } else {
        document.getElementById("imgL").src = "";
    }
    if (json.pageR) {
        displayFile(json.pageR);
        DisplayImg(json.mime, json.pageRBase64Str);
        document.getElementById("imgR").src = "data:" + json.mime + ";base64," + json.pageRBase64Str;
    } else {
        document.getElementById("imgR").src = "";
    }
    if (json.pageL || json.pageR) {
        audioCapture.play();
    }
}

var _path = null;
var _th = '<tr><th>File name</th><th>Operation</th></tr>';
var _tdOpration = '<td><a onclick="vf(this)">View</a> <a onclick="df(this)">Delete</a></td>';
function displayFile(file) {
    var pos = file.lastIndexOf('\\');
    if (_path == null) {
        _path = file.substring(0, pos);
    }
    var html = '<tr><td>' + file.substring(pos + 1) + '</td>' + _tdOpration + '</tr>';
    $("#tbFile").append(html);
}

//Add images
function DisplayImg(mime, imgBase64Str) {
    var h = '<div><img width="300" style="padding:5px;" src="' + 'data:' + mime + ';base64,' + imgBase64Str + '"></div>';
    $("#bottom_div").prepend(h);
}

// Clear the image list
function clearImageFile() {
    $("#image_data").empty();
    $("#tbFile").empty().append(_th);
}

function removeFinger() {
    if (!_setRemovalForeign) {
        var reqId = new Date().getTime() - 1;
        // Set remove finger RemovalForeign Foreign body removal 0- None 1- remove finger 2- remove binding hole.
        var cmd = JSON.stringify({func: 'SetCameraImageInfo', reqId:reqId, devNum:_devNum, removalForeign: 1});
        SendTxt(cmd);
        _setRemovalForeign = true;
    }
}

// Book Capture
function captureBook() {
    removeFinger();
    var reqId = new Date().getTime();
    // When capture a book, mode supports base64 and path. The path mode can respond to the message containing the path of the photo file, or the two modes can be used simultaneously.
    var cmd = JSON.stringify({func: 'CameraCaptureBook', reqId:reqId, devNum:_devNum, mode:'base64,path'});
    SendTxt(cmd);
}

function mergePDF() {
    var reqId = new Date().getTime();
    var fileArray = new Array();
    var trs = $('#tbFile tr');
    for (i = 1; i < trs.length; i++) {
        fileArray.push(_path + '\\' + trs[i].children[0].innerText);
    }
    // fileType: 0 PDF; 1-TIFF(not support)
    var cmd = JSON.stringify({func: 'MergeFile', reqId:reqId, fileType:0,imagePath:fileArray});
    SendTxt(cmd);
}

function vf(obj) {
    //var e = e || window.event;
    //var target = e.srcElement;//e.target ||
    //var par = $(target).parent(); 
    //var pre = par.prev();
    //var tex = pre.text();
    var file = _path + '\\' + $(obj).parent().prev().text();

    // Convert the file to base64, For display view
    var reqId = new Date().getTime();
    var cmd = JSON.stringify({func:'FileToBase64', reqId:reqId, filePath:file});
    SendTxt(cmd);
}

function DisplayFileView(v) {
    var img = document.getElementById("imgView");
    img.src = "data:" + v.mime + ";base64," + v.imgBase64Str;
    $("#divView").css('display', '').css('width', '').css('height', '');
    if (img.height > $("#divView").innerHeight()) {
        $("#divView").css('height', img.height + 'px');
    }
    if (img.width > $("#divView").innerWidth()) {
        $("#divView").css('width', img.width + 'px');
    }
}

// Delete the image file
function df(obj) {
    //var e = e || window.event;
    //var target = e.target || e.srcElement;
    $(obj).parent().parent().remove();
}

// Close view
function closeView() {
    $("#divView").css('display', 'none');
    document.getElementById("imgView").src = "";
}

function onSpeedChange() {
    var v = $("#speed").val();
    $("#speedNum").text(v);
    if (document.getElementById("autoCapture").checked) {
        sendAutoCaptureCmd(true, v);
    }
}

function OnAutoCaptureChange() {
    removeFinger();
    var v = $("#speed").val();
    sendAutoCaptureCmd(document.getElementById("autoCapture").checked, v);
}

// Send the command of automatic continuous.
function sendAutoCaptureCmd(enable, param) {
    var reqId = new Date().getTime();
    var cmd = JSON.stringify({func: 'CameraAutoCapture', reqId: reqId, "devNum": _devNum, enable: enable,
        isBook: true, detectionType: 0, param: param, mode: "base64,path"});
    // The CameraAutoCapture command is a multi-answer command when enabled (enable is true), except the response command (func is CameraAutoCapture)，
    // There will also be a response message with func for CameraCapture or CameraCaptureBook after the automatic shooting is successful。
    // Property "isBook" is true, the camera book will be processed consecutively and the response message is CameraCaptureBook
    SendTxt(cmd);
}