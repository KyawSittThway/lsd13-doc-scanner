var _url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
var _connected = false;
var _devNum = -1;
var _detectionType = -1; //auto capture type.
var _timer = 0;

Date.prototype.toLocaleString = function () {
    var y = this.getFullYear(), m = this.getMonth() + 1, d = this.getDate(), h = this.getHours(),
        mi = this.getMinutes(), s = this.getSeconds(), ms = this.getMilliseconds();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d) + ' ' + (h < 10 ? ('0' + h) : h) + ':' + (mi < 10 ? ('0' + mi) : mi) + ":" + (s < 10 ? ('0' + s) : s) + '.' + ((ms + 1000) + "").substr(1, 3);
};

/**
 * init webSocket connect
 */
function ConnectServer(callback, value) {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _connected = true;
        callback(value);
    }
    _ws.onclose = function (e) {
        _connected = false;
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            onTxtMessage(e.data);
        } else {
            onBinMessage(e);
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn);
    };
}

/**
 * Sends information to the server.
 */
function SendTxt(jsonStr) {
    _connected ? _ws.send(jsonStr) : ConnectServer(SendTxt, jsonStr)
}

/**
 * Process server binary messages.
 */
function onBinMessage(e) {
    console.log('onBinMessage', e);
}

/**
 * Process received server text messages.
 */
function onTxtMessage(msg) {
    var jsonObj = JSON.parse(msg);
    if (jsonObj.result != 0 && jsonObj.result != 9) {
        // If result=0 is returned, the execution is successful. If other values are returned, the execution is abnormal.9: The device is in use. Ignore this exception when you start the camera.
        console.log(jsonObj.func, jsonObj.errorMsg);
        alert(jsonObj.func + "\r\n" + jsonObj.errorMsg);
    } else {
        switch (jsonObj.func) {
            case "GetCameraVideoBuff":
                DisplayVideo(jsonObj);
                break;
            case "GetCameraInfo":
                DisplayDevInfo(jsonObj.devInfo);
                break;
            case "CameraCapture":
                DisplayCaptureImg(jsonObj);
                break;
            case "Notify":
                // receipt notify
                if (jsonObj.event == "OnUsbKeyPress") {
                    // Received notification of USB button press
                    // {"func":"Notify","event":"OnUsbKeyPress","reqId":1636099207839.0,"result":0,"time":"1636099214"}
                    // DisplayUsbKeyPress(jsonObj);
                } else if (jsonObj.event == "OnDeviceChanged") {
                    // The format of the device change is as follows:
                    // {"func":"Notify","event":"OnDeviceChanged","devName":"\\\\?\\USB#VID_0C45&PID_6366...","func":"OnDeviceChanged","result":0,"type":1}
                    // type: 1-Device insertion; 2-Device removed
                    GetCameraInfo();
                }
                break;
            case "CameraAutoCapture":
                DisplayStatus(jsonObj);
                break;
            default:
                console.log(msg);
                break;
        }
    }
}

// get device info.
function GetCameraInfo() {
    SendTxt('{"func":"GetCameraInfo","reqId":"' + new Date().getTime() + '"}');
}

//Display vide.
function DisplayVideo(v) {
    document.getElementById("video").src = "data:" + v.mime + ";base64," + v.imgBase64Str;
}

function DisplayDevInfo(devInfos) {
    if (devInfos == null || devInfos.length == 0) {
        alert(Lang.cam.doc);
        return;
    }
    if (_devNum < 0) {
        openCamera();
    }
}

function DisplayCaptureImg(json) {
    if (json.imgBase64 && json.imgBase64.length > 0) {
        document.getElementById("imgCapture").src = "data:" + json.mime + ";base64," + json.imgBase64[0];
        document.getElementById("captureInfo").innerText = "Auto capture success, " + new Date().toLocaleString();
        progress0.value = progress0.max;
        audioCapture.play();
    }
}

// Open camera.
function openCamera() {
    var reqId = new Date().getTime();
    if (_devNum >= 0) {
        //Close older device
        SendTxt('{"func":"CloseCamera","reqId":"' + reqId + '","devNum":' + _devNum + '}');
        reqId++;
    }

    //Open camera.
    _devNum = 0;
    SendTxt('{"func":"OpenCamera","reqId":"' + reqId + '","devNum":' + _devNum + ',"mediaNum":0,"resolutionNum":0,"fps":5}');
    reqId++;

    // Set cropping（cropping：0-No Cropping;1-Single Cropping; 2-Multiple Cropping）.
    SendTxt('{"func":"SetCameraImageInfo","reqId":"' + reqId + '","devNum":' + _devNum + ',"cropType":1}');
    reqId++;

    // Get Display video.
    SendTxt('{"func":"GetCameraVideoBuff","reqId":"' + reqId + '","devNum":' + _devNum + ',"enable":"true"}');
    reqId++;
}

// Auto burst control
function autoCaptureControl(type) {
    _detectionType = type;
    var reqId = new Date().getTime();
    var cmd;
    if (type < 0) {
        // Stop auto capture
        cmd = '{"func":"CameraAutoCapture","reqId":"' + reqId + '","devNum":' + _devNum + ',"enable":false}';
    } else {
        // The CameraAutoCapture command is a multi-answer command when enabled (enable is true), except the response command (func is CameraAutoCapture)，
        // There will also be a response message with funC for CameraCapture or CameraCaptureBook after the automatic shooting is successful。
        // Property "isBook" is true, the camera book will be processed consecutively and the response message is CameraCaptureBook.
        // Detection type: 0 monitoring mode, 1 timing mode.
        var paramVal = document.getElementById("speed").value;
        cmd = '{"func":"CameraAutoCapture","reqId":"' + reqId + '","devNum":' + _devNum + ',"enable":true,"isBook":false,"detectionType":' +
                type + ',"parma":' + paramVal + ',"mode":"base64"}';
    }
    SendTxt(cmd);

    // Timing tandem countdown (display only) control
    if (_detectionType == 1) {
        progress0.max = paramVal;
        progress0.value = 0;
        _timer = window.setInterval(function () {
            var v = progress0.value + 1;
            if (v > progress0.max) {
                v = 0;
            }
            progress0.value = v;
        }, 1000);
        document.getElementById("divTimer").style.display = '';
    } else {
        clearInterval(_timer);
        _timer = 0;
        document.getElementById("divTimer").style.display = 'none';
    }
}

function onSpeedChange() {
    var v = document.getElementById("speed").value;
    document.getElementById("speedNum").innerText = v;
    if (_detectionType >= 0)  {
        autoCaptureControl(_detectionType);
    }
}

function DisplayStatus(v) {
    if (v.enable) {
        $('#status').text(Lang.autoCapture.started + (v.detectionType == 1 ? Lang.autoCapture.timeLapse : Lang.autoCapture.auto));
    } else {
        $('#status').text(Lang.autoCapture.stopped);
    }
}

//Init info
$(function () {
    $('#status').text(Lang.autoCapture.notStart);
    //Get camera device list.
    GetCameraInfo();
});
