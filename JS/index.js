
$(document).ready(function(){ 
    var clientHeight = document.documentElement.clientHeight;
    var clientWidth = document.documentElement.clientWidth;
    var standWidth = 1920;
    var standHeight = 1080;
    var proportionWidth = clientWidth/standWidth;
    var proportionHeight = clientHeight/standHeight;
    $("#apiModuleBanner").css('height',(540*proportionHeight)+'px');
    $('#aiModuleContent').css('width',(1120*proportionWidth)+'px');
    $('#aiModuleContainer').css('paddingTop',(85*proportionHeight)+'px');
    $('#cmeraSDK').css('padding-right','0px');

    $("#icon1").css('height',(180*proportionHeight)+'px');
    $("#icon1").css('width',(180*proportionHeight)+'px');
    $("#cmeraSDKIcon").css('height',(183*proportionHeight)+'px');
    $("#cmeraSDKIcon").css('width',(183*proportionHeight)+'px');
    // $(".cmeraSDK").css('paddingRight','0px');
    $("#icon2").css('height',(180*proportionHeight)+'px');
    $("#icon2").css('width',(180*proportionHeight)+'px');
    $("#scannerSDKIcon").css('height',(183*proportionHeight)+'px');
    $("#scannerSDKIcon").css('width',(183*proportionHeight)+'px');
    $(".scannerSDK").css('paddingRight',(250*proportionWidth)+'px');
    $("#icon3").css('height',(180*proportionHeight)+'px');
    $("#icon3").css('width',(180*proportionHeight)+'px');
    $("#idCardSDKIcon").css('height',(66*proportionHeight)+'px');
    $("#idCardSDKIcon").css('width',(66*proportionHeight)+'px');

    GetSysVersion();
}); 

function pageJump(pageUrl){
    if(pageUrl == ""){
        alert(Lang.MSG.developing);
    }else{
        window.location.href = pageUrl;
    }
}

var _url = ((location.protocol==="https:") ? "wss://" : "ws://") + ((location.hostname === "") ? '127.0.0.1' : location.hostname) + ":" + ((location.port === "") ? '9000' : location.port);
var _ws;
function GetSysVersion() {
    if ('WebSocket' in window) {
        _ws = new WebSocket(_url);
    } else if (window.WebSocket) {
        _ws = new WebSocket(_url);
    } else if ('MozWebSocket' in window) {
        _ws = new MozWebSocket(_url);
    } else {
        alert(Lang.WS.lowVer);
    }
    _ws.onopen = function () {
        _ws.send('{"func":"GetVersion","reqId":"1"}');
    }
    _ws.onmessage = function (e) {
        if (typeof e.data === "string") {
            var jsonObj = JSON.parse(e.data);
            if (jsonObj.func == 'GetVersion') {
                // {"func":"GetVersion","reqId":"1","result":0,"sysVersion":20210907}
                $("#verInfo").text("Ver: " + jsonObj.sysVersion);
                _ws.close();
            }
        }
    }
    _ws.onerror = function (e) {
        alert(Lang.WS.disConn);
    }
}
